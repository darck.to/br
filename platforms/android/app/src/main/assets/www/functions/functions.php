<?php

	//GENERADOR DE CADENAS ALEATORIAS
	function generateRandomString($length) {
	    $characters = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZasdfgqwertzxcvbpoiuylkjhmn';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	//FOLIOS ORDERS
	function leeFolio() {
		//NOMBRE DE ARCHIVO
		$filename = '../../admin/assets/folio.json';
		if (file_exists($filename)) {
			$filename = file_get_contents($filename);
			$data = json_decode($filename, true);
			$folio = $data[0]['folio'];
		}
		return $folio;
	}
	function masFolio() {
		$filename = file_get_contents('../../admin/assets/folio.json');
		$data = json_decode($filename, true);
		$viejo = $data[0]['folio'];
		$data[0]['folio'] = $viejo + 1;
		//LO VOLVEMOS A GUARDAR
		$newJsonString = json_encode($data, JSON_PRETTY_PRINT);
		file_put_contents('../../admin/assets/folio.json', $newJsonString);
	}

	//FUNCTION COMPRUEBA EMAIL
  function validaEmail($correo) {
	  if (preg_match('/^[A-Za-z0-9-_.+%]+@[A-Za-z0-9-.]+\.[A-Za-z]{2,4}$/', $correo)) return true;
	    else return false;
	}

?>
