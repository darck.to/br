-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 03-12-2019 a las 00:39:18
-- Versión del servidor: 5.6.44-cll-lve
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `_auth_br`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cate_br`
--

CREATE TABLE `cate_br` (
  `id` int(11) NOT NULL COMMENT 'id de la tabla',
  `cate_index` varchar(12) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'indice de la categoria',
  `nom` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre de la categoria'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `cate_br`
--

INSERT INTO `cate_br` (`id`, `cate_index`, `nom`) VALUES
(2, 'STLWbB0SKH46', 'Departamentos'),
(3, 'ZmrBpTEzGdOZ', 'Casas'),
(4, '0ZeDbS4R8Qz2', 'Edificios'),
(5, 'bUV8N7rsJVLZ', 'Terrenos'),
(6, 'TQcQtEKChjzD', 'Residencias'),
(7, 'TI9lwWmdtz9u', 'Proyectos Urbanos'),
(8, 'YYTEwE29cVGD', 'Casas Moviles'),
(9, 'ADQefFfgLNLV', 'Fabricas'),
(10, '6vC2HV5YL64x', 'Preventa'),
(11, 'ixHvnfKLNDVN', 'Plazas'),
(12, 'AC5rFcF5tV7u', 'Locales'),
(13, 'iI0rK0BATFBT', 'Naves Industriales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `desc_br`
--

CREATE TABLE `desc_br` (
  `id` int(11) NOT NULL COMMENT 'id de la tabla',
  `pro_index` varchar(12) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'indice del producto',
  `des` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'descripcion'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dire_br`
--

CREATE TABLE `dire_br` (
  `id` int(11) NOT NULL COMMENT 'id de la tabla',
  `pro_index` varchar(12) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'indice del producto',
  `cal` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'calle',
  `num` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'numero exterior',
  `col` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'colonia',
  `ciu` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'ciudad',
  `cp` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'codigo postal',
  `est` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'estado/provincia',
  `pai` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'pais'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `init_auth`
--

CREATE TABLE `init_auth` (
  `id` int(11) NOT NULL COMMENT 'id de la tabla',
  `mai` text NOT NULL COMMENT 'email del usuario',
  `nom` text NOT NULL COMMENT 'nombre del usuario',
  `auth_number` varchar(16) NOT NULL COMMENT 'autorization number',
  `pas` text NOT NULL COMMENT 'password del usuario',
  `init_index` varchar(12) NOT NULL COMMENT 'init index'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `init_auth`
--

INSERT INTO `init_auth` (`id`, `mai`, `nom`, `auth_number`, `pas`, `init_index`) VALUES
(1, 'darck.to@gmail.com', 'Adm', 'xa9srQMsBWgtZApV', '827ccb0eea8a706c4c34a16891f84e7b', 'wUE1ot3A82tN'),
(2, 'valdezbeto@gmail.com', 'beto', 'OQXQSb6P0VePH9Gy', '827ccb0eea8a706c4c34a16891f84e7b', 'yksP4diQsRZj'),
(3, 'arturopartida54@gmail.com', 'whitetiger', 'jUEm6GatZyZ4HxU6', '1e5adfcda81f47773e1dc5035718d20e', 'ukEIbb0BfqGm');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perf_br`
--

CREATE TABLE `perf_br` (
  `id` int(11) NOT NULL COMMENT 'id de la tabla',
  `perf_index` varchar(12) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'indice del perfil',
  `nom` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre(s)',
  `ape` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'apellido paterno',
  `mat` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'apellido materno',
  `tel` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'telefono',
  `cel` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'celular',
  `plus_index` varchar(12) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'index del plan plus'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `perf_br`
--

INSERT INTO `perf_br` (`id`, `perf_index`, `nom`, `ape`, `mat`, `tel`, `cel`, `plus_index`) VALUES
(1, 'wUE1ot3A82tN', 'Beto', 'Perez', 'Landa', '5953003344', '1231231233', '1'),
(2, 'yksP4diQsRZj', 'Manuel', 'Valdez', 'Gutierres', '499455188', '4941055849', '1'),
(3, 'ukEIbb0BfqGm', 'Nombre', 'Apellido', 'Segundo Apellido', '', '', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pro_br`
--

CREATE TABLE `pro_br` (
  `id` int(11) NOT NULL COMMENT 'id de la tabla',
  `pro_index` varchar(12) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'indice del producto',
  `nom` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre del producto',
  `lat` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'latitud',
  `lng` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'longitud',
  `ren` tinyint(1) NOT NULL COMMENT 'renta',
  `pre` int(11) NOT NULL COMMENT 'precio / Costo',
  `cate_index` varchar(12) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'indice de la categoria',
  `perf_index` varchar(12) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'indice del perfil'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cate_br`
--
ALTER TABLE `cate_br`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `desc_br`
--
ALTER TABLE `desc_br`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dire_br`
--
ALTER TABLE `dire_br`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `init_auth`
--
ALTER TABLE `init_auth`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `perf_br`
--
ALTER TABLE `perf_br`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pro_br`
--
ALTER TABLE `pro_br`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cate_br`
--
ALTER TABLE `cate_br`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la tabla', AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `desc_br`
--
ALTER TABLE `desc_br`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la tabla';

--
-- AUTO_INCREMENT de la tabla `dire_br`
--
ALTER TABLE `dire_br`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la tabla';

--
-- AUTO_INCREMENT de la tabla `init_auth`
--
ALTER TABLE `init_auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la tabla', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `perf_br`
--
ALTER TABLE `perf_br`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la tabla', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `pro_br`
--
ALTER TABLE `pro_br`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la tabla';
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
