$(document).ready(function() {

  //ACTIONS HEADER
  $('.actionMainFilter').on('click', function(e){
    $('.overlay-sub').html('');
    $('.overlay-principal').html('');
    $(".navbar-menu").removeClass("is-active");
    $('.overlay-sub').load('templates/main/main-filter.html');
  })

  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function(e) {
    $('.overlay-sub').html('');
    $('.overlay-principal').html('');
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");
  });

  $('.showProductsCategories').on('click', function(e){
    $('.overlay-sub').html('');
    $('.overlay-principal').html('');
    $(".navbar-menu").toggleClass("is-active");
    $('.overlay-sub').load('templates/products/products-categories.html');
  });

  $('.showProductsSearch').on('click', function(e){
    $('.overlay-sub').html('');
    $('.overlay-principal').html('');
    $(".navbar-menu").toggleClass("is-active");
    $('.overlay-sub').load('templates/search/search.html');
  });

  $('.showPlanes').on('click', function(e){
    $('.overlay-sub').html('');
    $('.overlay-principal').html('');
    $(".navbar-menu").toggleClass("is-active");
    carga_modal();
    $('.modal-content').load('templates/planes/planes.html');
  });

  $('.changeCurrency').on('click', function(e) {
    var txt = $('.menu-symbol').html();
    if (txt == 'MXN') {
      localStorage.setItem('currency_symbol','USD');
      $('.menu-symbol').html('USD')
    } else if (txt == 'USD') {
      localStorage.setItem('currency_symbol','MXN');
      $('.menu-symbol').html('MXN')
    }
  })

  $('.init-logout').on('click', function(e) {
    $(this).addClass('is-loading');
    localStorage.removeItem("aUth_key");
    localStorage.removeItem("aUth_user");
    setTimeout(location.reload.bind(location), 1000);
  })

  translate();

});
