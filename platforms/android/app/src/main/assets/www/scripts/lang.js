$(document).ready(function() {

  var dictionary = {
    'user_header_h2': { //USER TEMPLATE
      'en': 'Edit your data',
      'es': 'Edita tu Informacion'
    },
    'user_header_h3': {
      'en': 'edit only the data you need to change',
      'es': 'modifica solo la informacion que quieras cambiar'
    },
    'user_button_form': {
      'en': 'Edit',
      'es': 'Editar'
    },
    'user_input_usu': {
      'en': 'User',
      'es': 'Usuario'
    },
    'user_input_mai': {
      'en': 'Email',
      'es': 'Email'
    },
    'user_input_pas': {
      'en': 'Password',
      'es': 'Contrasena'
    },
    'user_input_pac': {
      'en': 'Confirm Password',
      'es': 'Confirm Contrasena'
    },
    'user_input_nom': {
      'en': 'Name',
      'es': 'Nombre'
    },
    'user_input_ape': {
      'en': 'Last Name',
      'es': 'Apellido Paterno'
    },
    'user_input_mat': {
      'en': 'Last Name',
      'es': 'Apellido Materno'
    },
    'user_input_tel': {
      'en': 'Telephone',
      'es': 'Telefono'
    },
    'user_input_cel': {
      'en': 'Cellular',
      'es': 'Celular'
    },//SEARCH TEMPLATE
    'search_button_can': {
      'en': 'Cancel',
      'es': 'Cancelar'
    },
    'search_button_bus': {
      'en': 'Search',
      'es': 'Buscar'
    },
    'search_h1_cat': {
      'en': 'Categories',
      'es': 'Categorias'
    },
    'search_h1_pai': {
      'en': 'Country',
      'es': 'Pais'
    },
    'search_h1_ciu': {
      'en': 'City',
      'es': 'Ciudad'
    },
    'search_h1_opc': {
      'en': 'Options',
      'es': 'Opciones'
    },
    'search_h1_pre': {
      'en': 'Price ',
      'es': 'Precio '
    },
    'search_span_des': {
      'en': 'from: $',
      'es': 'desde: $'
    },
    'search_h1_has': {
      'en': 'To: $',
      'es': 'Hasta: $'
    },
    'search_h1_sal': {
      'en': 'Exit',
      'es': 'Salir'
    },
    'search_h1_bus': {
      'en': 'Search',
      'es': 'Busqueda'
    },
    'search_h1_fil': {
      'en': 'Filters',
      'es': 'Filtros'
    }, //PRODUCTS add TEMPLATE
    'products_h1_can': {
      'en': 'Cancel',
      'es': 'Cancelar'
    },
    'products_h1_agr': {
      'en': 'Add',
      'es': 'Agregar'
    },
    'products_h1_gua': {
      'en': 'Save',
      'es': 'Guardar'
    },
    'products_li_ubi': {
      'en': 'Ubication',
      'es': 'Ubicacion'
    },
    'products_li_opc': {
      'en': 'Options',
      'es': 'Opciones'
    },
    'products_li_fot': {
      'en': 'Photos',
      'es': 'Fotografias'
    },
    'products_li_des': {
      'en': 'Description',
      'es': 'Descripcion'
    },
    'products_input_tit': {
      'en': 'Title',
      'es': 'Titulo'
    },
    'products_input_cal': {
      'en': 'Street',
      'es': 'Calle'
    },
    'products_input_col': {
      'en': 'Neighborhood',
      'es': 'Colonia'
    },
    'products_input_ciu': {
      'en': 'City',
      'es': 'Ciudad'
    },
    'products_input_est': {
      'en': 'State',
      'es': 'Estado'
    },
    'products_input_pai': {
      'en': 'Country',
      'es': 'Pais'
    },
    'products_h1_opc': {
      'en': 'Options',
      'es': 'Opciones'
    },
    'products_h1_fot': {
      'en': 'Photos',
      'es': 'Fotografias'
    },
    'products_h1_des': {
      'en': 'Description',
      'es': 'Descripcion'
    },
    'products_text_des': {
      'en': 'Input your property description',
      'es': 'Ingresa la descripcion de tu propiedad'
    },
    'products_input_ren': {
      'en': 'Lease',
      'es': 'Renta'
    },
    'products_input_ven': {
      'en': 'Sale',
      'es': 'Venta'
    },
    'products_h1_pre': {
      'en': 'Price',
      'es': 'Precio'
    },
    'products_input_pre': {
      'en': 'Price',
      'es': 'Precio'
    },
    'products_h1_cat': {
      'en': 'Categories',
      'es': 'Categorias'
    },
    'products_button_pub': {
      'en': 'Publish',
      'es': 'Publicar'
    },
    'products_h1_sal': {
      'en': 'Exit',
      'es': 'Salir'
    },
    'products_h1_pro': {
      'en': 'Property',
      'es': 'Propiedad'
    },
    'products_h1_fav': {
      'en': 'Favorite',
      'es': 'Favorito'
    },
    'products_small_pre': {
      'en': 'Press the marker to trace a route',
      'es': 'Presiona el marcador para trazar una ruta'
    },
    'products_h1_dir': {
      'en': 'Address',
      'es': 'Direccion'
    },
    'products_a_con': {
      'en': 'Contact',
      'es': 'Contactar'
    }, //MAIN USER template
    'main_h6_lis': {
      'en': 'Property List',
      'es': 'Listado de Propiedades'
    },
    'main_h6_map': {
      'en': 'Map',
      'es': 'Mapa'
    },
    'main_h6_per': {
      'en': 'Profile',
      'es': 'Perfil'
    },
    'main_p_pre': {
      'en': 'Press to edit',
      'es': 'Presiona para editar'
    },
    'main_h1_pro': {
      'en': 'Properties',
      'es': 'Propiedades'
    },
    'main_p_man': {
      'en': 'Press and hold on elements to edit them',
      'es': 'Manten presionado sobre las propiedades para editarlas'
    },
    'main_h1_fil': {
      'en': 'Filters',
      'es': 'Filtros'
    }, //MAIN nav template
    'main_h1_cer': {
      'en': 'Close to you',
      'es': 'Cerca de ti'
    },
    'main_h5_anu': {
      'en': 'properties close to you',
      'es': 'propiedades cerca de ti'
    },
    'main_a_bus': {
      'en': 'Search',
      'es': 'Buscar'
    },
    'main_a_cat': {
      'en': 'Categories',
      'es': 'Categorias'
    },
    'main_a_plu': {
      'en': 'Plus Plan',
      'es': 'Plan Plus'
    },
    'main_a_mon': {
      'en': 'Change Currency',
      'es': 'Cambiar Moneda'
    },
    'main_a_lan': {
      'en': 'Change Language',
      'es': 'Cambiar Lenguaje'
    },
    'main_a_esp': {
      'en': 'ENG',
      'es': 'ESP'
    },
    'main_a_cer': {
      'en': 'Log out',
      'es': 'Cerrar sesion'
    },//MAIN FILTER template
    'main_button_can': {
      'en': 'Cancel',
      'es': 'Cancelar'
    },
    'main_button_gua': {
      'en': 'Save',
      'es': 'Guardar'
    },
    'main_h1_dis': {
      'en': 'Distance',
      'es': 'Distancia'
    },
    'main_h1_opc': {
      'en': 'Options',
      'es': 'Opciones'
    },
    'main_h1_pre': {
      'en': 'Price ',
      'es': 'Precio '
    },
    'main_span_des': {
      'en': 'from: $0 to: $',
      'es': 'desde: $0 hasta: $'
    },
    'main_h6_cle': {
      'en': 'Clean Filter',
      'es': 'Limpiar Filtro'
    },
    'main_h6_fav': {
      'en': 'Save Alert',
      'es': 'Guardar Alerta'
    }, //INIT register template
    'init_h1_reg': {
      'en': 'Register an account',
      'es': 'Resgistra una cuenta'
    },
    'init_h1_ini': {
      'en': 'Log In',
      'es': 'Inicia Sesion'
    },
    'init_input_usu': {
      'en': 'User name',
      'es': 'Nombre de usuario'
    },
    'init_input_mai': {
      'en': 'Email',
      'es': 'Correo Electronico'
    },
    'init_input_con': {
      'en': 'Password',
      'es': 'Contrasena'
    },
    'init_input_pac': {
      'en': 'Confirm your password',
      'es': 'Confirma tu contrasena'
    },
    'init_input_cup': {
      'en': 'Coupon',
      'es': 'Cupon'
    },
    'init_button_reg': {
      'en': 'Register',
      'es': 'Registrar'
    },
    'init_button_log': {
      'en': 'Log in',
      'es': 'Iniciar sesion'
    }, //USER user edit js
    'init_mod_h1': {
      'en': 'Enter your email to recover your password',
      'es': 'Introduce tu correo para recuperar tu contraseña'
    },
    'init_mod_spa': {
      'en': 'We\'ll send you a new password to your inbox',
      'es': 'Enviaremos un nuevo password a tu buzon de correo'
    },
    'init_mod_inp': {
      'en': 'Email',
      'es': 'Email'
    },
    'init_mod_but': {
      'en': 'Send',
      'es': 'Enviar'
    },
    'init_h6_rec': {
      'en': 'Recover Password',
      'es': 'Recuperar Contraseña'
    },
    'error_ajax': {
      'en': 'Conection timed out, please retry',
      'es': 'Conexion superada, por favor reintenta'
    }, //SEARCH search js
    'search_option_cat': {
      'en': 'All categories',
      'es': 'Todas las categorias'
    },
    'search_option_pai': {
      'en': 'All countries',
      'es': 'Todos los paises'
    },
    'search_option_ciu': {
      'en': 'All cities',
      'es': 'Todas las ciudades'
    },
    'search_h1_err_ran': {
      'en': 'Price "FROM" must be less than Price "TO"',
      'es': 'El precio "DESDE" debe ser menor al precio "HASTA"'
    }, //PRODUCTS products-show js
    'products_h1_due': {
      'en': 'Owner',
      'es': 'Propietario'
    },
    'products_h1_gal': {
      'en': 'Gallery',
      'es': 'Galeria'
    },
    'products_opc_cua': {
      'en': 'Rooms',
      'es': 'Cuartos'
    },
    'products_opc_jac': {
      'en': 'Jacuzzi',
      'es': 'Jacuzzi'
    },
    'products_opc_air': {
      'en': 'Air Conditining',
      'es': 'Aire Acondicionado'
    },
    'products_opc_jar': {
      'en': 'Front Yard',
      'es': 'Jardin Delantero'
    },
    'products_opc_tra': {
      'en': 'Back Yard',
      'es': 'Jardin Trasero'
    },
    'products_opc_chi': {
      'en': 'Chimmey',
      'es': 'Chimenea'
    },
    'products_opc_ban': {
      'en': 'Bathrooms',
      'es': 'Banos'
    },
    'products_opc_coc': {
      'en': 'Garage',
      'es': 'Cochera'
    },
    'products_opc_pis': {
      'en': 'Pool',
      'es': 'Piscina'
    },
    'products_opc_ter': {
      'en': 'Terrace',
      'es': 'Terraza'
    },
    'products_opc_bal': {
      'en': 'Balcony',
      'es': 'Balcon'
    },
    'products_opc_seg': {
      'en': 'Security',
      'es': 'Seguridad'
    },
    'products_opc_rec': {
      'en': 'Reception',
      'es': 'Recepcion'
    },
    'products_opc_gim': {
      'en': 'Gym',
      'es': 'Gimnasio'
    },
    'products_opc_yes': {
      'en': 'Yes',
      'es': 'Si'
    },
    'products_opc_ves': {
      'en': 'cars',
      'es': 'vehiculos'
    },
    'products_opc_veh': {
      'en': 'car',
      'es': 'vehiculo'
    },
    'products_h2_sen': {
      'en': 'Send a message',
      'es': 'Envia un mensaje'
    },
    'products_h3_sel': {
      'en': 'Select a platform',
      'es': 'Selecciona una plataforma'
    },
    'products_cat_pub': {
      'en': 'Publication',
      'es': 'Publicacion'
    },
    'products_cat_pus': {
      'en': 'Publications',
      'es': 'Publicaciones'
    },
    'products_h6_wha': {
      'en': 'Whatsapp',
      'es': 'Whatsapp'
    },
    'products_h6_sms': {
      'en': 'SMS',
      'es': 'Mensaje'
    },
    'products_h6_cel': {
      'en': 'Cellphone',
      'es': 'Celular'
    },
    'products_h6_ofi': {
      'en': 'Office',
      'es': 'Oficina'
    },//PRODUCTS add js
    'products_edit_press': {
      'en': 'Change location pressing on another point of the map',
      'es': 'Puedes modificar la ubicacion seleccionando otro punto en el mapa'
    },
    'products_edit_del': {
      'en': 'Delete',
      'es': 'Borrar'
    },
    'products_confirm_del': {
      'en': 'Do you wish to delete this property?',
      'es': '¿Deseas eliminar esta propiedad?'
    }, //buttons main
    'main-button-add': {
      'en': 'Add Propierty',
      'es': 'Agregar Propiedad'
    },
    'main-button-search': {
      'en': 'Area search',
      'es': 'Busqueda en el area'
    }, //TOASTS
    'toast_filter_save': {
      'en': 'You will receive notifications',
      'es': 'Recibiras notificaciones'
    }
  };

  var langs = ['en', 'es'];
  var current_lang_index = 0;
  var current_lang = langs[current_lang_index];

  window.change_lang = function() {
    if (current_lang_index == 0) {
      current_lang_index = 1;
      localStorage.setItem('lang',1)
    } else {
      current_lang_index = 0;
      localStorage.setItem('lang',0)
    }
    current_lang = langs[current_lang_index];
    translate();
  };

  window.translate = function() {
    $('[data-translate]').each(function(){
      var key = $(this).data('translate');
      $(this).html(dictionary[key][current_lang] || 'N/A');
    });
    $('[data-placeholder]').each(function(){
      var key = $(this).data('placeholder');
      $(this).attr('placeholder', dictionary[key][current_lang] || 'N/A');
    });
  };

  window.translate_string = function(string) {
    var key = string;
    return(dictionary[key][current_lang] || 'N/A');
  };

  translate()

})
