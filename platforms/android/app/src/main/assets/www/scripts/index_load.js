$(document).ready(function() {

  $(document).on('focus',':input', function(){
    $(this).attr('autocomplete','off');
  });

  // onSuccess Callback
  // This method accepts a Position object, which contains the
  // current GPS coordinates
  //
  var onSuccess = function(position) {
    localStorage.setItem("lat", position.coords.latitude);
    localStorage.setItem("lng", position.coords.longitude);

    //BACK BUTTON X2 TO EXIT credits to https://lloydzhou.github.io/project/2014/04/30/phonegap-exit-on-double-click-backbutton
    document.addEventListener('deviceready', function() {
      var exitApp = false, intval = setInterval(function (){exitApp = false;}, 1000);
      document.addEventListener("backbutton", function (e){
        e.preventDefault();
        if (exitApp) {
          clearInterval(intval)
          (navigator.app && navigator.app.exitApp()) || (device && device.exitApp())
        }
        else {
          exitApp = true;
          $('.overlay-sub').html('');
          $('.overlay-principal').html('');
          $('.overlay-full').html('');
          if ($('.modal').length) {
            $('.modal').remove();
          };
          $(".navbar-burger").removeClass("is-active");
          $(".navbar-menu").removeClass("is-active");
        }
      }, false);
    }, false)
  };

  // onError Callback receives a PositionError object
  //
  function onError(error) {
    alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
  }

  navigator.geolocation.getCurrentPosition(onSuccess, onError);

  //CREA UN LOADER PARA TODOS Y TODO
  window.carga_loader = function(e) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').remove();
    }
    //CREA UN MODAL NUEVO
    var content = '<i class="fas fa-5x fa-compass has-text-white pulsating-icon"></i>';
    var modal = '<div class="modal">';
    modal += '<div class="modal-background"></div>';
    modal += '<div class="modal-content">';
    modal += '<h1 class="has-text-centered">' + content + '</div>';
    modal += '</div>';
    modal += '</div>';
    $('body').prepend(modal);
    $('.modal').css('z-index','9000');
    $('.modal').toggleClass("is-active");
  }
  window.cierra_loader = function(e) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').remove();
    }
  }

  //CREA UN TOAST MODAL
  window.toast = function(e) {
    var toast = '<span id="toast" class="tag is-dark">' + e + '</span>';
    $('body').append(toast);
    setTimeout(function(){
      if ($('#toast').length > 0) {
        $('#toast').remove();
      }
    }, 5000)
  }

  //CREA UN MODAL PARA TODOS Y TODO
  window.carga_modal = function(incoming) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').remove();
    }
    //CREA UN MODAL NUEVO
    var content = incoming;
    var modal = '<div class="modal">';
    modal += '<div class="modal-background"></div>';
    modal += '<div class="modal-content">';
    modal += content;
    modal += '</div>';
    modal += '<button class="modal-close is-large" aria-label="close"></button>';
    modal += '</div>';
    $('body').prepend(modal);
    $('.modal').css('z-index','9000');
    $('.modal').toggleClass("is-active");
    $('.modal-close').on('click', function(){
      $('.modal').toggleClass("is-active");
    });
    $('.modal-background').on('click', function(){
      $('.modal').toggleClass("is-active");
    })
  }
  //ROTATE
  $.fn.animateRotate = function(angle, duration, easing, complete) {
    return this.each(function() {
      var $elem = $(this);

      $({deg: 0}).animate({deg: angle}, {
        duration: duration,
        easing: easing,
        step: function(now) {
          $elem.css({
            transform: 'rotate(' + now + 'deg)'
          });
        },
        complete: complete || $.noop
      });
    });
  };

  //windows.CLOSEALL
  window.closeall = function(e) {
    //$('.overlay-sub').html('');
    //$('.overlay-principal').html('');
    //$('.overlay-full').html('');
    //if ($('.modal').length) {
      //$('.modal').remove();
    //};
    //$(".navbar-burger").removeClass("is-active");
    //$(".navbar-menu").removeClass("is-active");
    //setTimeout(location.reload.bind(location), 500);
  }

  window.currency_get = function(e) {
    var result;
    //CAMBIA TIPO DE CAMBIO DE ACUERDO A LA API https://api.exchangeratesapi.io/latest?base=MXN&symbols=USD
    $.ajax({
      method: 'get',
      url: 'https://api.exchangeratesapi.io/latest?base=MXN&symbols=USD',
      dataType: "json",
      context: document.body,
      cache: false,
      success: function(data) {
        $.each(data, function (name, value) {
          result = value.USD;
          return false
        });
        localStorage.setItem('currency',result);
      },
      error: function(xhr, tst, err) {
        alert('El tiempo de espera fue superado, por favor intentalo en un momento mas');
        closeall();
      },
      timeout: 10000
    });
  }

  window.money_format = function (x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  carga_main_login();

});

function carga_main_login(e) {
  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  comprueba_login(stringaUth_key,stringaUth_user);
  function comprueba_login(a,u) {
    carga_loader();
    $.ajax({
        type: "POST",
        url: "php/init/init-comprueba-auth.php",
        data: {
          auth : a,
          user : u
        },
        async:true,
        dataType : 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
        success: function(data) {
          var aprobacion
          $.each(data, function (name, value) {
            aprobacion = value.success;
          });
          if (aprobacion == true) {
            carga_main_nav();
          } else {
            $('.overlay-principal').load('templates/init/init-form.html');
          }
          localStorage.setItem("range", 24); //Millas
          cierra_loader()
        },
        error: function(xhr, tst, err) {
          alert('El tiempo de espera fue superado, por favor intentalo en un momento mas');
          closeall();
        },
        timeout: 10000
    })

  }
};

function carga_main_nav(e) {
  $('.header-principal').load('templates/main/main-nav.html');
  $(".navbar-burger").click(function() {
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");
  })
  //CARGA EL NAVIGATOR
  carga_main_body();
  carga_main_scroll();
  //CARGA EL NAVIGATOR
  carga_main_button();
  //CARGA EL MAP
  carga_main_tabs();
}


function carga_main_body(e) {
  $('.cuerpo-principal').load('templates/main/main-map.html');
}

function carga_main_scroll() {
  $('.overlay-principal').load('templates/main/main-scroll.html');
}

function carga_main_tabs(e) {
  $('.navigator-principal').load('templates/main/main-tabs.html');
}

function carga_main_button(e) {
  $('.button-principal').load('templates/main/main-buttons.html');
}
