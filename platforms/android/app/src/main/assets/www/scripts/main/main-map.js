$(document).ready(function() {

  carga_main_map();
  translate();

});

function carga_main_map(e) {
  if (map) {
  } else {
    var map = L.map('mainmap').setZoom(13);
  }

  var iconNear = L.icon({
    iconUrl: 'img/leaf/near.png',
    iconSize: [35, 35]
  });

  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: ''
  }).addTo(map);

  map.on("moveend", function () {
    $('#button-search').removeClass('is-hidden');
  });

  $('#button-search').on('click', function(e) {
    coords = L.latLng(map.getCenter());
    lat = coords.lat;
    lng = coords.lng;
    localStorage.setItem("lat",lat);
    localStorage.setItem("lng",lng);
    newCenter();
  })

  //NUEVO CENTRO
  function newCenter() {
    carga_loader();
    var latNow = localStorage.getItem("lat");
    var lngNow = localStorage.getItem("lng");

    //BUSCA COINCIDENCIAS EN 50KLM A LA REDONDA
    var dis = localStorage.getItem("range");
    dis = dis * 1.609344; //KLM

    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    $.ajax({
      type: "POST",
      url: "php/main/main-coincidencias.php",
      data: {
        auth : stringaUth_key,
        user : stringaUth_user,
        lat : latNow,
        lng : lngNow,
        dis : dis
      },
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        n = 0;
        $.each(data, function (name, value) {
          if (value.success == false) {
          } else {
            n++;
            L.marker([value.lat, value.lng], {icon: iconNear}).addTo(map).on('click', function(e) {
              carga_product(value.pro_index);
            });
          }
        });
        $('#noNear').html(n);
        cierra_loader();
      },
      error: function(xhr, tst, err) {
        alert('El tiempo de espera fue superado, por favor intentalo en un momento mas');
        closeall();
      },
      timeout: 10000
    })

  }

  //SIN FILTRO
  function onLocationFound(e) {
    carga_loader();
    var latNow = localStorage.getItem("lat");
    var lngNow = localStorage.getItem("lng");

    L.marker(e.latlng).addTo(map);

    //BUSCA COINCIDENCIAS EN 50KLM A LA REDONDA
    var dis = localStorage.getItem("range");
    dis = dis * 1.609344; //KLM

    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    $.ajax({
      type: "POST",
      url: "php/main/main-coincidencias.php",
      data: {
        auth : stringaUth_key,
        user : stringaUth_user,
        lat : latNow,
        lng : lngNow,
        dis : dis
      },
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        n = 0;
        $.each(data, function (name, value) {
          if (value.success == false) {
          } else {
            n++;
            L.marker([value.lat, value.lng], {icon: iconNear}).addTo(map).on('click', function(e) {
              carga_product(value.pro_index);
            });
          }
        });
        $('#noNear').html(n);
        cierra_loader();
      },
      error: function(xhr, tst, err) {
        alert('El tiempo de espera fue superado, por favor intentalo en un momento mas');
        closeall();
      },
      timeout: 10000
    })

  }

  //CON FILTRO
  function onLocationFilter(e) {
    var latNow = localStorage.getItem("lat");
    var lngNow = localStorage.getItem("lng");

    L.marker(e.latlng).addTo(map);

    //BUSCA COINCIDENCIAS EN 50KLM A LA REDONDA
    var dis = localStorage.getItem("range");
    dis = dis * 1.609344; //KLM

    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");

    var n = 0;
    var marcadores = JSON.parse(localStorage.getItem('markers'));
    $.each(marcadores, function (name, value) {
      n++;
      L.marker([value.lat, value.lng], {icon: iconNear}).addTo(map).on('click', function(e) {
        carga_product(value.pro_index);
        //$('.overlay-full').load('templates/products/products-show.html');
      });
    });
    $('#noNear').html(n);

  }

  function onLocationError(e) {
    alert(e.message);
  }
  var filtros = localStorage.getItem("filter");
  if (filtros == "false") {
    map.on('locationfound', onLocationFound);
    map.on('locationerror', onLocationError);
  } else {
    map.on('locationfound', onLocationFilter);
    map.on('locationerror', onLocationError);
  }

  map.locate({setView: true, maxZoom: 16});
}

function carga_product(e) {
  var pro_index = e;
  localStorage.setItem('proshow',pro_index);
  $('.overlay-full').load('templates/products/products-show.html');
}
