$(document).ready(function() {

  $('.exitOverlay').on('click', function(e){
    $('.overlay-sub').html('');
  });

  $('.showFilters').on('click', function(e){
    $('#search-filter').removeClass('is-hidden');
    $('.overlay-fs').addClass('is-hidden');
    carga_filtros();
  })

  var slider =  document.getElementById('slider1');
  var output = document.getElementById('number1');
  output.innerHTML = slider.value;

  slider.oninput = function() {
    output.innerHTML = money_format(this.value);
  };

  var slider2 =  document.getElementById('slider2');
  var output2 = document.getElementById('number2');
  output2.innerHTML = slider2.value;

  slider2.oninput = function() {
    output2.innerHTML = money_format(this.value);
  };

  carga_busqueda();

  translate();

});

function carga_busqueda(e) {
  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  var cate_index;

  if (localStorage.getItem("cate_index") === null) {
    cate_index = null;
  } else {
    cate_index = localStorage.getItem('cate_index');
  }

  if (e) {
    var form_data = new FormData();
    $.each(e, function (name, value) {
      form_data.append(name, value);
    });
    form_data.append('complex', "yes");
    form_data.append('auth', stringaUth_key);
    form_data.append('user', stringaUth_user);
    form_data.append('cate_index', cate_index);
  } else {
    var form_data = new FormData();
    form_data.append('complex', "no");
    form_data.append('auth', stringaUth_key);
    form_data.append('user', stringaUth_user);
    form_data.append('cate_index', cate_index);
  }

  $.ajax({
    type: "POST",
    url: "php/search/search.php",
    data: form_data,
    contentType: false,
    processData: false,
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $('#search-filter').addClass('is-hidden');
      $('.overlay-fs').removeClass('is-hidden');
      $('#search-scroll').html('');
      var box = "";
      box += '<div class="box ma-one"></div>';
      $.each(data, function (name, value) {
        if (value.success) {
          box += '<div class="box ma-b-half ma-lr-one pa-no product-show" id=' + value.pro_index + '>';
            box += '<div class="columms ma-no pa-no">';
              box += '<div class="columm ma-no pa-no">';
                box += '<img class="main-scroll-overlay-img" src="assets/pro_img/' + value.pro_index  + '/' + value.pro_index  + '_0.png"/>';
              box += '</div>';
              box += '<div class="columm pa-one">';
                box += '<div class="content">';
                  box += '<h1 class="is-size-4 fo-w-l">' + value.nom + '</h1>';
                  box += '<div class="columns ma-no-b pa-no-b">';
                    box += '<div class="column has-text-centered">';
                      if (parseInt(value.cuartos) > 0) { bg = 'has-background-grey-dark';} else {bg = '';}
                      box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                        box += '<i class="has-text-white fas fa-cube"></i>';
                      box += '</div>';
                      if (value.jacuzzi == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                      box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                        box += '<i class="has-text-white fas fa-hot-tub"></i>';
                      box += '</div>';
                      if (value.air == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                      box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                        box += '<i class="has-text-white fas fa-wind"></i>';
                      box += '</div>';
                      if (value.jardin == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                      box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                        box += '<i class="has-text-white fas fa-tree"></i>';
                      box += '</div>';
                      if (value.trasero == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                      box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                        box += '<i class="has-text-white fas fa-bath"></i>';
                      box += '</div>';
                      if (value.chimenea == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                      box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                        box += '<i class="has-text-white fas fa-fire-alt"></i>';
                      box += '</div>';
                      if (parseInt(value.banos) > 0) { bg = 'has-background-grey-dark';} else {bg = '';}
                      box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                        box += '<i class="has-text-white fas fa-lock"></i>';
                      box += '</div>';
                      if (parseInt(value.cochera) > 0) { bg = 'has-background-grey-dark';} else {bg = '';}
                      box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                        box += '<i class="has-text-white fas fa-car-side"></i>';
                      box += '</div>';
                      if (value.piscina == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                      box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                        box += '<i class="has-text-white fas fa-swimmer"></i>';
                      box += '</div>';
                      if (value.terraza == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                      box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                        box += '<i class="has-text-white fab fa-microsoft"></i>';
                      box += '</div>';
                      if (value.balcon == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                      box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                        box += '<i class="has-text-white fas fa-person-booth"></i>';
                      box += '</div>';
                      if (value.seguridad == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                      box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                        box += '<i class="has-text-white fas fa-user-shield"></i>';
                      box += '</div>';
                      if (value.recepcion == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                      box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                        box += '<i class="has-text-white fas fa-concierge-bell"></i>';
                      box += '</div>';
                      if (value.gimnasio == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                      box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                        box += '<i class="has-text-white fas fa-dumbbell"></i>';
                      box += '</div>';
                    box += '</div>';
                  box += '</div>';
                box += '</div>';
              box += '</div>';
            box += '</div>';
          box += '</div>';
        } else {
          var list = '<div class="has-background-white ma-half pa-one bo-r-five">';
            list += '<h1 class="has-text-centered ma-one is-size-1 fo-w-l">Sin resultados</h1>';
            list += '<h1 class="has-text-centered pa-one"><i class="has-text-danger far fa-4x fa-frown-open"></i></h1>';
          list += '</div>';
          carga_modal();
          $('.modal-content').html(list);
        }
      });
      box += '<div class="box ma-one"></div>';
      $('#search-scroll').html(box);

      $('.product-show').on('click', function(e){
        $('.overlay-principal').html('');
        var pro_index = $(this).attr('id');
        localStorage.setItem('proshow',pro_index);
        $('.overlay-full').load('templates/products/products-show.html');
      })

      //LIMPIA LOS FILTROS
      if (localStorage.getItem("cate_index") === null) {
      } else {
        localStorage.removeItem('cate_index');
      }
    },

    error: function(xhr, tst, err) {
      //alert('El tiempo de espera fue superado, por favor intentalo en un momento mas');
      //closeall();
    },
    timeout: 10000
  })
}

//CARGA LOS FILTROS
function carga_filtros(e) {

  $('.exitSearchOverlay').on('click', function(e){
    $('#search-filter').addClass('is-hidden');
    $('.overlay-fs').removeClass('is-hidden');
  });

  carga_opciones();

  carga_categorias();
  carga_paises();

  function carga_opciones() {
    $.getJSON('assets/opc_br/opc_br.json',function(data){
      $('#product-opc').html('');
      var opc = "";
      $.each(data, function(id,content){
        opc += '<div class="filter-options opt-' + content.id + '" data-text="' + content.name + '">';
          opc += '<i class="has-text-white ' + content.icon + '"></i>';
          if (content.type == "integer") { opc += '<input class="input is-hidden integer-input input-opc-filter int-' + content.id + '" type="number" value="1">';}
        opc += '</div>';
        opc += '<span class="is-hidden"><small class="has-text-white-ter">' + content.name + '</small></span>';
      })
      $('#product-opc').append(opc);
      //OPCIONES
      $('.filter-options').on('click', function(e){
        $(this).toggleClass('has-background-grey-dark count');
        $('.filter-options').children('.input-opc-filter').addClass('is-hidden');
        $(this).children('.input-opc-filter').removeClass('is-hidden');
        toast($(this).data('text'))
      });
      $(".integer-input").on("keypress keyup blur",function (event) {
         $(this).val($(this).val().replace(/[^\d].+/, ""));
          if ((event.which < 48 || event.which > 57)) {
              event.preventDefault();
          }
      })
    })
  }

  function carga_categorias(e) {
    $.ajax({
      type: "POST",
      url: "php/products/products-categorias.php",
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        $('#categorias-select').html('');
        $('#categorias-select').append('<option value=0>' + translate_string('search_option_cat') + '</option>');
        $.each(data, function (name, value) {
          $('#categorias-select').append('<option value=' + value.cate_index + '>' + value.nom + '&nbsp<b class="hast-text-grey">(' + value.num + ')</b></option>');
        });
        $('#categorias-select').toggleClass('is-loading');
      },
      error: function(xhr, tst, err) {
        alert(translate_string('error_ajax'));
        closeall();
      },
      timeout: 10000
    })
  }

  function carga_paises(e) {
    $.ajax({
      type: "POST",
      url: "php/products/products-paises.php",
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        $('#paises-select').html('');
        $('#paises-select').append('<option value=0>' + translate_string('search_option_pai') + '</option>');
        $.each(data, function (name, value) {
          $('#paises-select').append('<option value=' + value.pai + '>' + value.pai + '&nbsp<b class="hast-text-grey">(' + value.num + ')</b></option>');
        });
        $('#paises-select').toggleClass('is-loading');

        $('#paises-select').on('change',function(){
          var pai = $(this).val();
          carga_ciudades(pai);
        })
      },
      error: function(xhr, tst, err) {
        alert(translate_string('error_ajax'));
        closeall();
      },
      timeout: 10000
    })
  }

  function carga_ciudades(e) {
    var pai = e;
    $.ajax({
      type: "POST",
      url: "php/products/products-ciudades.php",
      data: {
        pai : pai
      },
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        $('#ciudades-select').html('');
        $('#ciudades-select').append('<option value=0>' + translate_string('search_option_ciu') + '</option>');
        $.each(data, function (name, value) {
          if (value.success) {
            $('#ciudades-select').append('<option value=' + value.ciu + '>' + value.ciu + '&nbsp<b class="hast-text-grey">(' + value.num + ')</b></option>');
          }
        });
        $('#ciudades-select').toggleClass('is-loading');
      },
      error: function(xhr, tst, err) {
        alert(translate_string('error_ajax'));
        closeall();
      },
      timeout: 10000
    })
  }

}

function guarda_search_filter(e) {
  var mod = false;
  var list = '<div class="has-background-white ma-half pa-one bo-r-five">';
  list += '<h1 class="fo-w-l has-text-centered has-text-danger">' + translate_string('search_h1_err_ran') + '</h1>';
  list += '</div>';

  var ren = $('input[name="ren"]:checked').val();

  var desde = parseInt($('#slider1').val());
  var hasta = parseInt($('#slider2').val());

  if (desde >= hasta) {
    mod = true
  }

  if (mod) {
    carga_modal();
    $('.modal-content').html(list);
    return false
  }

  var categorias = $('#categorias-select').val();
  var paises = $('#paises-select').val();
  var ciudades = $('#ciudades-select').val();

  var opciones;
  if ($('.count').length == 0) { opciones = false} else { opciones = true};

  if ($('.opt-cuartos').hasClass('count')) { var cuartos = $('.int-cuartos').val(); } else { var cuartos = 0; }
  if ($('.opt-jacuzzi').hasClass('count')) { var jacuzzi = true; } else { var jacuzzi = false; }
  if ($('.opt-air').hasClass('count')) { var air = true; } else { var air = false; }
  if ($('.opt-jardin').hasClass('count')) { var jardin = true; } else { var jardin = false; }
  if ($('.opt-chimenea').hasClass('count')) { var chimenea = true; } else { var chimenea = false; }
  if ($('.opt-banos').hasClass('count')) { var banos = $('.int-banos').val(); } else { var banos = 0; }
  if ($('.opt-cochera').hasClass('count')) { var cochera = $('.int-cochera').val(); } else { var cochera = 0; }
  if ($('.opt-piscina').hasClass('count')) { var piscina = true; } else { var piscina = false; }
  if ($('.opt-terraza').hasClass('count')) { var terraza = true; } else { var terraza = false; }
  if ($('.opt-balcon').hasClass('count')) { var balcon = true; } else { var balcon = false; }
  if ($('.opt-seguridad').hasClass('count')) { var seguridad = true; } else { var seguridad = false; }
  if ($('.opt-recepcion').hasClass('count')) { var recepcion = true; } else { var recepcion = false; }
  if ($('.opt-gimnasio').hasClass('count')) { var gimnasio = true; } else { var gimnasio = false; }

  var form_data = {categorias: categorias, renta: ren, paises: paises, ciudades: ciudades, desde: desde, hasta: hasta, opciones: opciones, cuartos: cuartos, jacuzzi: jacuzzi, air: air, jardin: jardin, chimenea: chimenea, banos: banos, cochera: cochera, piscina: piscina, terraza: terraza, balcon: balcon, seguridad: seguridad, recepcion: recepcion, gimnasio: gimnasio};
  carga_busqueda(form_data);

}
