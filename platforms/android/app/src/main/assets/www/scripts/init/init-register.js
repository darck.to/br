$(document).ready(function() {

  $('.buttonLoginForm').on('click', function(e){
    $('.overlay-principal').load('templates/init/init-form.html');
  });

  //REGISTRAMOS EL USUARIO
  $('#initRegisterUser').submit(function(event) {
    carga_loader();
    event.preventDefault();

    var formData = new FormData(document.getElementById("initRegisterUser"));
    var formMethod = $(this).attr('method');
    var rutaScrtip = $(this).attr('action');

    var request = $.ajax({
      url: rutaScrtip,
      method: formMethod,
      data: formData,
      contentType: false,
      processData: false,
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
    });
    // handle the responses
    request.done(function(data) {
      var aprobacion, key, user;
      $.each(data, function (name, value) {
        aprobacion = value.success;
        key = value.aUth_key;
        user = value.aUth_user;
      });
      if (aprobacion == true) {
        $('.signButton').addClass('is-loading');
        localStorage.setItem("aUth_key", key);
        localStorage.setItem("aUth_user", user);
        setTimeout(location.reload.bind(location), 1000);
      } else {
        console.log(data.error);
      };
      cierra_loader();
    })
    request.fail(function(jqXHR, textStatus) {
      console.log(textStatus);
    })
    request.always(function(data) {
      // clear the form
      $('#initRegisterUser').trigger('reset');
    });

  });

  translate();

});

//EMAIL
$('#r-mai').on('keyup', function() {
	email = $(this).val();
	if (validateEmail(email)) {
    $('.signButton').prop('disabled',false);
	} else {
    $('.signButton').prop('disabled',true);
	}

  if (this.value.length == 0) {
    $('.signButton').prop('disabled',true);
  }
});

//CONFIMATION
$('#r-pac').on('keyup', function() {
	pass = $('#r-pas').val();
	confirm = $(this).val();
	if (confirm != pass) {
    $('.signButton').prop('disabled',true);
	} else {
    $('.signButton').prop('disabled',false);
	}

  if (this.value.length == 0) {
  	$('.signButton').prop('disabled',true);
  }
});
$('#r-cup').on('keyup',function(){
  var value = $(this).val();
  if(value.length == 12) {
    var coupon = $(this).val();
    $.ajax({
      type: "POST",
      url: "php/init/init-coupon.php",
      data: {
        coupon : coupon
      },
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        $.each(data,function(index,content){
          if(content.succes == true) {
            $('#r-cup').css('background-color','#00D1B2');
            $('#r-pla').val(2);
          } else {
            $('#r-cup').css('background-color','#FF3860');
            $('#r-pla').val(1);
          }
        })
      },
      error: function(xhr, tst, err) {
        alert(translate_string('error_ajax'));
      }
    });
  }
})

function validateEmail(sEmail) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {
		return true;
	}
	else {
		return false;
	}
}
