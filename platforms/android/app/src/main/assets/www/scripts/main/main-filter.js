$(document).ready(function() {

  $('#slider1').val(localStorage.getItem("range"));
  var oldPrice = localStorage.getItem("price");
  if (oldPrice > 0) {
    oldPrice = money_format(oldPrice);
    $('#slider2').val(oldPrice);
  } else {
    $('#slider2').val(money_format(10));
  }

  $('.exitFilterOverlay').on('click', function(e){
    $('.overlay-sub').html('');
  });

  var slider =  document.getElementById('slider1');
  var output = document.getElementById('number1');
  output.innerHTML = slider.value;

  slider.oninput = function() {
    output.innerHTML = this.value;
  };

  var slider2 =  document.getElementById('slider2');
  var output2 = document.getElementById('number2');
  //output2.innerHTML = slider2.value;

  slider2.oninput = function() {
    output2.innerHTML = money_format(this.value);
  };

  carga_opciones();
  carga_categorias();

  function carga_opciones() {
    $.getJSON('assets/opc_br/opc_br.json',function(data){
      $('#product-opc').html('');
      var opc = "";
      $.each(data, function(id,content){
        opc += '<div class="filter-options opt-' + content.id + '" data-text="' + content.name + '">';
          opc += '<i class="has-text-white ' + content.icon + '"></i>';
          if (content.type == "integer") { opc += '<input class="input is-hidden integer-input input-opc-filter int-' + content.id + '" type="number" value="1">';}
        opc += '</div>';
        opc += '<span class="is-hidden"><small class="has-text-white-ter">' + content.name + '</small></span>';
      })
      $('#product-opc').append(opc);
      //OPCIONES
      $('.filter-options').on('click', function(e){
        $(this).toggleClass('has-background-grey-dark count');
        $('.filter-options').children('.input-opc-filter').addClass('is-hidden');
        $(this).children('.input-opc-filter').removeClass('is-hidden');
        toast($(this).data('text'))
      });
    });
    $(".integer-input").on("keypress keyup blur",function (event) {
       $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    })
  }

  function carga_categorias(e) {
    $.ajax({
      type: "POST",
      url: "php/products/products-categorias.php",
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        $('#categorias-select').html('');
        $('#categorias-select').append('<option value=0>' + translate_string('search_option_cat') + '</option>');
        $.each(data, function (name, value) {
          $('#categorias-select').append('<option value=' + value.cate_index + '>' + value.nom + '&nbsp<b class="hast-text-grey">(' + value.num + ')</b></option>');
        });
        $('#categorias-select').toggleClass('is-loading');
      },
      error: function(xhr, tst, err) {
        alert(translate_string('error_ajax'));
        closeall();
      },
      timeout: 10000
    })
  }

  //filter erase
  $('.filter-erase').on('click', function(e){
    localStorage.setItem("filter", false);
    localStorage.setItem("range", 24);
    localStorage.setItem("price", 500000);
    localStorage.removeItem("markers");
    $('#slider1').val(localStorage.getItem("range"));
    var cleanPrice = money_format(localStorage.getItem("price"));
    $('#slider2').val(cleanPrice);
    carga_main_body();
    $('.overlay-sub').html('');
  })

  $('.filter-save').on('click', function(e){
    $(this).addClass('has-text-warning');

    carga_loader();

    var ren = $('input[name="ren"]:checked').val();

    var mod = false;
    var list = '<div class="has-background-white ma-half pa-one bo-r-five">';
    list += '<h1 class="fo-w-l has-text-centered has-text-danger">Debes seleccionar cuando menos una opcion:</h1>';
    list += '<ul>';

    if ($('.count').length == 0) {list += '<li>No hay opciones seleccionadas</li>'; mod = true; };
    var categorias = $('#categorias-select').val()

    if ($('.opt-cuartos').hasClass('count')) { var cuartos = $('.int-cuartos').val(); } else { var cuartos = 0; }
    if ($('.opt-jacuzzi').hasClass('count')) { var jacuzzi = true; } else { var jacuzzi = false; }
    if ($('.opt-air').hasClass('count')) { var air = true; } else { var air = false; }
    if ($('.opt-jardin').hasClass('count')) { var jardin = true; } else { var jardin = false; }
    if ($('.opt-chimenea').hasClass('count')) { var chimenea = true; } else { var chimenea = false; }
    if ($('.opt-banos').hasClass('count')) { var banos = $('.int-banos').val(); } else { var banos = 0; }
    if ($('.opt-cochera').hasClass('count')) { var cochera = $('.int-cochera').val(); } else { var cochera = 0; }
    if ($('.opt-piscina').hasClass('count')) { var piscina = true; } else { var piscina = false; }
    if ($('.opt-terraza').hasClass('count')) { var terraza = true; } else { var terraza = false; }
    if ($('.opt-balcon').hasClass('count')) { var balcon = true; } else { var balcon = false; }
    if ($('.opt-seguridad').hasClass('count')) { var seguridad = true; } else { var seguridad = false; }
    if ($('.opt-recepcion').hasClass('count')) { var recepcion = true; } else { var recepcion = false; }
    if ($('.opt-gimnasio').hasClass('count')) { var gimnasio = true; } else { var gimnasio = false; }

    list += '</ul>';
    list += '</div>';

    if (mod) {
      carga_modal();
      $('.modal-content').html(list);
      return false;
    }

    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var precio = $('#slider2').val();
    $.ajax({
      type: "POST",
      url: "php/main/main-filter-guarda.php",
      data: {
        auth : stringaUth_key,
        user : stringaUth_user,
        ren : ren,
        precio : precio,
        categorias : categorias,
        cuartos : cuartos,
        jacuzzi : jacuzzi,
        air : air,
        jardin : jardin,
        chimenea : chimenea,
        banos : banos,
        cochera : cochera,
        piscina : piscina,
        terraza : terraza,
        balcon : balcon,
        seguridad : seguridad,
        recepcion : recepcion,
        gimnasio : gimnasio
      },
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      success: function(data) {
        function returnDelay(){
          cierra_loader();
          carga_main_body();
          $('.overlay-sub').html('');
        }
        setTimeout(returnDelay, 1000);
        toast(translate_string('toast_filter_save'));
      },
      error: function(xhr, tst, err) {
        alert('El tiempo de espera fue superado, por favor intentalo en un momento mas');
        closeall();
      },
      timeout: 10000
    })
  })

  translate();

})

function guarda_filtro_main(e) {
  carga_loader();

  var ren = $('input[name="ren"]:checked').val();
  var categorias = $('#categorias-select').val()

  var mod = false;
  var list = '<div class="has-background-white ma-half pa-one bo-r-five">';
  list += '<h1 class="fo-w-l has-text-centered has-text-danger">Debes seleccionar cuando menos una opcion:</h1>';
  list += '<ul>';

  if ($('.count').length == 0) {list += '<li>No hay opciones seleccionadas</li>'; mod = true};

  if ($('.opt-cuartos').hasClass('count')) { var cuartos = $('.int-cuartos').val(); } else { var cuartos = 0; }
  if ($('.opt-jacuzzi').hasClass('count')) { var jacuzzi = true; } else { var jacuzzi = false; }
  if ($('.opt-air').hasClass('count')) { var air = true; } else { var air = false; }
  if ($('.opt-jardin').hasClass('count')) { var jardin = true; } else { var jardin = false; }
  if ($('.opt-chimenea').hasClass('count')) { var chimenea = true; } else { var chimenea = false; }
  if ($('.opt-banos').hasClass('count')) { var banos = $('.int-banos').val(); } else { var banos = 0; }
  if ($('.opt-cochera').hasClass('count')) { var cochera = $('.int-cochera').val(); } else { var cochera = 0; }
  if ($('.opt-piscina').hasClass('count')) { var piscina = true; } else { var piscina = false; }
  if ($('.opt-terraza').hasClass('count')) { var terraza = true; } else { var terraza = false; }
  if ($('.opt-balcon').hasClass('count')) { var balcon = true; } else { var balcon = false; }
  if ($('.opt-seguridad').hasClass('count')) { var seguridad = true; } else { var seguridad = false; }
  if ($('.opt-recepcion').hasClass('count')) { var recepcion = true; } else { var recepcion = false; }
  if ($('.opt-gimnasio').hasClass('count')) { var gimnasio = true; } else { var gimnasio = false; }

  list += '</ul>';
  list += '</div>';

  if (mod) {
    carga_modal( );
    $('.modal-content').html(list);
    return false;
  }

  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  var lat = localStorage.getItem("lat");
  var lng = localStorage.getItem("lng");
  var distancia = $('#slider1').val();
  distancia = distancia  * 1.609344; //millas a KLM
  var precio = $('#slider2').val();
  $.ajax({
    type: "POST",
    url: "php/main/main-filter.php",
    data: {
      auth : stringaUth_key,
      user : stringaUth_user,
      lat : lat,
      lng : lng,
      distancia : distancia,
      ren : ren,
      categorias : categorias,
      precio : precio,
      cuartos : cuartos,
      jacuzzi : jacuzzi,
      air : air,
      jardin : jardin,
      chimenea : chimenea,
      banos : banos,
      cochera : cochera,
      piscina : piscina,
      terraza : terraza,
      balcon : balcon,
      seguridad : seguridad,
      recepcion : recepcion,
      gimnasio : gimnasio
    },
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    success: function(data) {
      localStorage.setItem("range", distancia/1.609344); //Millas a Klm
      localStorage.setItem("price", precio); //Millas a Klm
      localStorage.setItem("filter", true); //filter
      localStorage.setItem("markers", JSON.stringify(data)); //markers
      carga_main_body();
      $('.overlay-sub').html('');
      cierra_loader();
    },
    error: function(xhr, tst, err) {
      alert('El tiempo de espera fue superado, por favor intentalo en un momento mas');
      closeall();
    },
    timeout: 10000
  })
}
