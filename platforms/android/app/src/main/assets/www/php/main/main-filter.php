<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);
  $lat = mysqli_real_escape_string($mysqli,$_POST['lat']);
  $lng = mysqli_real_escape_string($mysqli,$_POST['lng']);
  $distancia = mysqli_real_escape_string($mysqli,$_POST['distancia']);
  $renta = mysqli_real_escape_string($mysqli, $_POST['ren']);
  $cate_index = mysqli_real_escape_string($mysqli,$_POST['cate_index']);
  $precio = mysqli_real_escape_string($mysqli, $_POST['precio']);
  $cuartos = mysqli_real_escape_string($mysqli, $_POST['cuartos']);
  $jacuzzi = mysqli_real_escape_string($mysqli, $_POST['jacuzzi']);
  $air = mysqli_real_escape_string($mysqli, $_POST['air']);
  $jardin = mysqli_real_escape_string($mysqli, $_POST['jardin']);
  $chimenea = mysqli_real_escape_string($mysqli, $_POST['chimenea']);
  $banos = mysqli_real_escape_string($mysqli, $_POST['banos']);
  $cochera = mysqli_real_escape_string($mysqli, $_POST['cochera']);
  $piscina = mysqli_real_escape_string($mysqli, $_POST['piscina']);
  $terraza = mysqli_real_escape_string($mysqli, $_POST['terraza']);
  $balcon = mysqli_real_escape_string($mysqli, $_POST['balcon']);
  $seguridad = mysqli_real_escape_string($mysqli, $_POST['seguridad']);
  $recepcion = mysqli_real_escape_string($mysqli, $_POST['recepcion']);
  $gimnasio = mysqli_real_escape_string($mysqli, $_POST['gimnasio']);

  $consulta_cate = "";
  if (strlen($_POST['categorias']) == 12) {
    $consulta_cate = " AND cate_index = '" . $cate_index . " ";
  }

  $sql_auth =  $mysqli->query("SELECT init_index, nom FROM init_auth WHERE auth_number = '".$auth."' AND nom = '".$user."' ");
  if ($sql_auth->num_rows > 0) {
    $row = $sql_auth->fetch_assoc();

    $sqlCo =  $mysqli->query("SELECT id, lat, lng, pre, pro_index FROM (SELECT *, (((acos(sin((".$lat."*pi()/180)) * sin((lat*pi()/180))+cos((".$lat."*pi()/180)) * cos((lat*pi()/180)) * cos(((".$lng."- lng)*pi()/180))))*180/pi())*60*1.1515*1.609344) as distance FROM pro_br)pro_br WHERE distance <= $distancia AND ren = $renta $consulta_cate LIMIT 50 ");
    if ($sqlCo->num_rows > 0) {
      while ($rowCo = $sqlCo->fetch_assoc()) {
        //DESPUES DE LAS COORDENADAS Y LA DISTANCIA EN KILOMETROS, EVALUAMOS EL PRECIO
        if ($rowCo['pre'] <= $precio) {
          //EVALUAMOS
          $filename = file_get_contents('../../assets/opc_br/'.$rowCo['pro_index'].'_opc.json');
        	$data = json_decode($filename, true);

          if ($cuartos) {
            if ($data[0]['cuartos']) {
              $resultados[] = array("success"=>true, "pro_index"=>$rowCo['pro_index'], 'lat'=>$rowCo['lat'], 'lng'=>$rowCo['lng']);
            } else {
              $resultados[] = array("success"=>false, "error"=>'Error, cuartos');
            }
          } elseif ($jacuzzi) {
            if ($data[0]['jacuzzi']) {
              $resultados[] = array("success"=>true, "pro_index"=>$rowCo['pro_index'], 'lat'=>$rowCo['lat'], 'lng'=>$rowCo['lng']);
            } else {
              $resultados[] = array("success"=>false, "error"=>'Error, jacuzzi');
            }
          } elseif ($air) {
            if ($data[0]['air']) {
              $resultados[] = array("success"=>true, "pro_index"=>$rowCo['pro_index'], 'lat'=>$rowCo['lat'], 'lng'=>$rowCo['lng']);
            } else {
              $resultados[] = array("success"=>false, "error"=>'Error, air');
            }
          } elseif ($jardin) {
            if ($data[0]['jardin'] || $data[0]['trasero']) {
              $resultados[] = array("success"=>true, "pro_index"=>$rowCo['pro_index'], 'lat'=>$rowCo['lat'], 'lng'=>$rowCo['lng']);
            } else {
              $resultados[] = array("success"=>false, "error"=>'Error, jardin');
            }
          } elseif ($chimenea) {
            if ($data[0]['$chimenea']) {
              $resultados[] = array("success"=>true, "pro_index"=>$rowCo['pro_index'], 'lat'=>$rowCo['lat'], 'lng'=>$rowCo['lng']);
            } else {
              $resultados[] = array("success"=>false, "error"=>'Error, $chimenea');
            }
          } elseif ($banos) {
            if ($data[0]['banos']) {
              $resultados[] = array("success"=>true, "pro_index"=>$rowCo['pro_index'], 'lat'=>$rowCo['lat'], 'lng'=>$rowCo['lng']);
            } else {
              $resultados[] = array("success"=>false, "error"=>'Error, banos');
            }
          } elseif ($cochera) {
            if ($data[0]['cochera']) {
              $resultados[] = array("success"=>true, "pro_index"=>$rowCo['pro_index'], 'lat'=>$rowCo['lat'], 'lng'=>$rowCo['lng']);
            } else {
              $resultados[] = array("success"=>false, "error"=>'Error, cochera');
            }
          } elseif ($piscina) {
            if ($data[0]['piscina']) {
              $resultados[] = array("success"=>true, "pro_index"=>$rowCo['pro_index'], 'lat'=>$rowCo['lat'], 'lng'=>$rowCo['lng']);
            } else {
              $resultados[] = array("success"=>false, "error"=>'Error, piscina');
            }
          } elseif ($terraza) {
            if ($data[0]['terraza']) {
              $resultados[] = array("success"=>true, "pro_index"=>$rowCo['pro_index'], 'lat'=>$rowCo['lat'], 'lng'=>$rowCo['lng']);
            } else {
              $resultados[] = array("success"=>false, "error"=>'Error, terraza');
            }
          } elseif ($balcon) {
            if ($data[0]['jardin']) {
              $resultados[] = array("success"=>true, "pro_index"=>$rowCo['pro_index'], 'lat'=>$rowCo['lat'], 'lng'=>$rowCo['lng']);
            } else {
              $resultados[] = array("success"=>false, "error"=>'Error, jardin');
            }
          } elseif ($seguridad) {
            if ($data[0]['seguridad']) {
              $resultados[] = array("success"=>true, "pro_index"=>$rowCo['pro_index'], 'lat'=>$rowCo['lat'], 'lng'=>$rowCo['lng']);
            } else {
              $resultados[] = array("success"=>false, "error"=>'Error, seguridad');
            }
          } elseif ($recepcion) {
            if ($data[0]['recepcion']) {
              $resultados[] = array("success"=>true, "pro_index"=>$rowCo['pro_index'], 'lat'=>$rowCo['lat'], 'lng'=>$rowCo['lng']);
            } else {
              $resultados[] = array("success"=>false, "error"=>'Error, recepcion');
            }
          } elseif ($gimnasio) {
            if ($data[0]['gimnasio']) {
              $resultados[] = array("success"=>true, "pro_index"=>$rowCo['pro_index'], 'lat'=>$rowCo['lat'], 'lng'=>$rowCo['lng']);
            } else {
              $resultados[] = array("success"=>false, "error"=>'Error, gimnasio');
            }
          }

        }
      }
    } else {
      $resultados[] = array("success"=>false, "error"=>'Error, por favor contacta soporte');
    }

    print json_encode($resultados);

  } else {
    print json_encode('Error');
  }

  include('../../functions/cierra_conexion.php');
?>
