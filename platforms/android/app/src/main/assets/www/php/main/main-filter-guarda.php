<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $ren =  mysqli_real_escape_string($mysqli, $_POST['ren']);
  $categorias = mysqli_real_escape_string($mysqli, $_POST['categorias']);
  $precio = mysqli_real_escape_string($mysqli, $_POST['precio']);
  $cuartos = mysqli_real_escape_string($mysqli, $_POST['cuartos']);
  $jacuzzi = mysqli_real_escape_string($mysqli, $_POST['jacuzzi']);
  $air = mysqli_real_escape_string($mysqli, $_POST['air']);
  $jardin = mysqli_real_escape_string($mysqli, $_POST['jardin']);
  $chimenea = mysqli_real_escape_string($mysqli, $_POST['chimenea']);
  $banos = mysqli_real_escape_string($mysqli, $_POST['banos']);
  $cochera = mysqli_real_escape_string($mysqli, $_POST['cochera']);
  $piscina = mysqli_real_escape_string($mysqli, $_POST['piscina']);
  $terraza = mysqli_real_escape_string($mysqli, $_POST['terraza']);
  $balcon = mysqli_real_escape_string($mysqli, $_POST['balcon']);
  $seguridad = mysqli_real_escape_string($mysqli, $_POST['seguridad']);
  $recepcion = mysqli_real_escape_string($mysqli, $_POST['recepcion']);
  $gimnasio = mysqli_real_escape_string($mysqli, $_POST['gimnasio']);

  $sql_auth =  $mysqli->query("SELECT init_index, nom FROM init_auth WHERE auth_number = '".$auth."' AND nom = '".$user."' ");
  if ($sql_auth->num_rows > 0) {
    $row = $sql_auth->fetch_assoc();

    $sql_perf = $mysqli->query("SELECT `plus_index` FROM `perf_br` WHERE `perf_index` = '".$row['init_index']."'");
    if ($sql_perf->num_rows > 0) {
      $row_perf = $sql_perf->fetch_assoc();
      $filename = '../../assets/plus_br/paq_' . $row_perf['plus_index'] . '.json';
      $filename = file_get_contents($filename);
			$json = json_decode($filename, true);
      foreach ($json as $content) {
        $filter = $content['filt'];
      }
    }

    //ingresamos los datos al JSON categorias
    $cabecera[] = array('id'=> $filter, 'renta'=>$ren, 'categorias'=>$categorias, 'precio'=>$precio, 'cuartos'=>$cuartos, 'jacuzzi'=>$jacuzzi, 'air'=>$air, 'jardin'=> $jardin, 'chimenea'=> $chimenea, 'banos'=> $banos, 'cochera'=> $cochera, 'piscina'=> $piscina, 'terraza'=> $terraza, 'balcon'=> $balcon, 'seguridad'=> $seguridad, 'recepcion'=> $recepcion, 'gimnasio'=> $gimnasio, 'mail'=>true);
    //CUENTA LOS FILTROS Y LOS COMPARA CON LOS DEL PLAN
    $fileList = glob("../../assets/filt_br/" . $row["init_index"] . "/" . $row["init_index"] . "_*.json");
    if (count($fileList) < $filter) {
      $filename = '../../assets/filt_br/'.$row['init_index'].'/'.$row['init_index'].'_' . (count($fileList) + 1) . '.json';
      $newJsonString = json_encode($cabecera, JSON_PRETTY_PRINT);
      file_put_contents($filename, $newJsonString);
      $resultados[] = array("success"=>true, "info"=>"Filtro Guardado");
    } else {
      $resultados[] = array("success"=>false, "error"=>'Error, debes borrar un filtro en tu perfil');
    }
    print json_encode($resultados);

  } else {
    print json_encode('Error');
  }

  include('../../functions/cierra_conexion.php');
?>
