<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');

  // "limpiamos" los campos del formulario de posibles códigos maliciosos
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);
  $num = mysqli_real_escape_string($mysqli,$_POST['num']);

  $sql =  $mysqli->query("SELECT init_index, nom FROM init_auth WHERE auth_number = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $init_index = $row['init_index'];

    // Set new file name
    $new_image_name = "../../assets/pro_img/pre/".$init_index."_".$num.".png";
    if (file_exists($new_image_name)) {
      unlink($new_image_name);
    }
    // upload file
    if (move_uploaded_file($_FILES["file"]["tmp_name"], $new_image_name)) {
      $resultados[] = array("success"=>true, "img"=>$new_image_name,$auth,$user);
    } else {
      $resultados[] = array("error"=>'El archivo no pudo subirse, por favor contacta soporte');
    }

  } else {
    $resultados[] = array("success"=> false);
  }

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../functions/cierra_conexion.php');
?>
