<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $sql_auth =  $mysqli->query("SELECT init_index, nom FROM init_auth WHERE auth_number = '".$auth."' AND nom = '".$user."' ");
  if ($sql_auth->num_rows > 0) {
    $row = $sql_auth->fetch_assoc();
    $init_index = $row['init_index'];

    $sql_perf =  $mysqli->query("SELECT nom, ape, mat, tel, cel FROM perf_br WHERE perf_index = '".$init_index."' ");
    if ($sql_perf->num_rows > 0) {
      $row_perf = $sql_perf->fetch_assoc();
      if (empty($row_perf['nom'])) { $nom = "Nombre";} else { $nom = $row_perf['nom'];}
      if (empty($row_perf['ape'])) { $ape = "Apellido";} else { $ape = $row_perf['ape'];}
      if (empty($row_perf['mat'])) { $mat = "Segundo Apellido";} else { $mat = $row_perf['mat'];}
      if (empty($row_perf['tel'])) { $tel = "Telefono";} else { $tel = $row_perf['tel'];}
      if (empty($row_perf['cel'])) { $cel = "Celular";} else { $cel = $row_perf['cel'];}

      $sql_init =  $mysqli->query("SELECT mai FROM init_auth WHERE init_index = '".$init_index."' ");
      if ($sql_init->num_rows > 0) {
        $row_init = $sql_init->fetch_assoc();
        if (empty($row_init['mai'])) { $mai = "EMAIL";} else { $mai = $row_init['mai'];}
      } else {
        $resultados[] = array("error"=>'Error, por favor contacta soporte');
      }
      $avatar = '../../assets/perf_img/'.$init_index.'.png';
      if (file_exists($avatar)) {$avatar = 'assets/perf_img/'.$init_index.'.png';} else {$avatar = 'assets/perf_img/avatar.png';}
      $resultados[] = array("img"=>$avatar, "nom"=>$nom, 'ape'=>$ape, 'mat'=>$mat, 'tel'=>$tel, 'cel'=>$cel, "mai"=>$mai);

    } else {
      $resultados[] = array("error"=>'Error, por favor contacta soporte');
    }


    print json_encode($resultados);

  } else {
    print json_encode('Error');
  }


  include('../../functions/cierra_conexion.php');
?>
