<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $sql_auth =  $mysqli->query("SELECT init_index FROM init_auth WHERE auth_number = '".$auth."' AND nom = '".$user."' ");
  if ($sql_auth->num_rows > 0) {
    $row = $sql_auth->fetch_assoc();
    $init_index = $row['init_index'];

    //CARGAMOS EL PLAN DEL USUARIO
    $sqlPerf =  $mysqli->query("SELECT `plus_index` FROM `perf_br` WHERE `perf_index` = '".$init_index."'");
    if ($sqlPerf->num_rows > 0) {
      $rowPerf = $sqlPerf->fetch_assoc();
      $planPerf = $rowPerf['plus_index'];
    }

    $fileList = glob('../../assets/plus_br/paq_*.json');
    //RECORREMOS LOS ARCHIVOS json
    //RECORREMOS LOS ARCHIVOS
  	foreach($fileList as $filename) {
  	  //SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
  		if (file_exists($filename)) {
  			$filename = file_get_contents($filename);
  			$json = json_decode($filename, true);
  			foreach ($json as $content) {
          $resultados[] = array('success'=>true, 'price'=>$content['price'], 'plus_index'=>$content['plus_index'], 'opc'=>$content['opc'], 'mult'=>$content['mult'], 'filt'=>$content['filt'], 'pub'=>$content['pub'], 'exp'=>$content['exp'], 'user_plan'=>$planPerf);
  			}
  		}
  	}

  } else {
    $resultados[] = array('success'=>false, 'error'=>'Contacta soporte tecnico');
  }

  print json_encode($resultados);

  include('../../functions/cierra_conexion.php');
?>
