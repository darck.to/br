<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');

  $new_image_name = urldecode($_FILES["file"]["name"]);
  if (move_uploaded_file($_FILES["file"]["tmp_name"], "assets/perf_img/".$new_image_name)) {
    print json_encode('Se subio: '.$new_image_name);
  } else {
    print json_encode('No subio: '.$new_image_name);
  }
?>
