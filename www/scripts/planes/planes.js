$(document).ready(function() {

  //CONSULTA EL TIPO DE CAMBIO
  currency_get();
  carga_planes_info();
  translate();

});

function carga_planes_info(e) {
  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  $.ajax({
    type: "POST",
    url: "php/planes/planes-info.php",
    data: {
      auth : stringaUth_key,
      user : stringaUth_user
    },
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $('#planes-info').html('');
      var symbol = localStorage.getItem('currency_symbol');
      var content = '';
      var title = 'Basico';
      var headcolor = 'link';
      var boton = '';
      var n = 0;
      var usd = localStorage.getItem('currency');
      var coin, notice = '';
      $.each(data, function (name, value) {
        content += '<div class="box ma-one pa-no">';
          content += '<ul class="menu-list has-text-centered">';
            if (value.plus_index == 2) { title = 'Plus'; headcolor = 'danger';}
            content += '<li class="has-background-' + headcolor + ' has-text-white is-size-4 pa-one width-100">Plan ' + title + '</li>';
            if (localStorage.getItem('currency_symbol') == 'USD') {coin = value.price * usd } else { coin = value.price }
            content += '<li class="has-background-success has-text-white is-size-6 pa-half">' + translate_string('search_h1_pre') + ': $<span class="t-price-' + n + '" val=' + coin + '>' + coin.toFixed(2) + '</span>&nbsp;<span class="t-symbol">' + symbol + '</span>&nbsp;<small>/mensual</small></li>';
            content += '<li class="pa-half">No. de Opciones por Publicacion: ' + value.opc + '</li>';
            content += '<li class="pa-half">No. de Fotografias por Publicacion: ' + value.mult + '</li>';
            content += '<li class="pa-half">No. de Filtros para Guardar: ' + value.filt + '</li>';
            content += '<li class="pa-half">Publicaciones: ' + value.pub + '</li>';
            content += '<li class="pa-half">Publicaciones: ' + value.exp + '</li>';
            if (value.user_plan != value.plus_index && value.user_plan == 1) { boton = '<button class="button is-primary planes-pago">Comprar Plan</button>'; notice = '<li class="pa-half is-size-7 has-text-right">El pago es procesado por Mercado Pago, sigue las instrucciones despues de presionar el boton</li>'}
            content += '<li class="pa-half">' + boton + '</li>';
            content += notice;
          content += '</ul>';
        content += '<div>'
        $('#planes-info').append(content);
        content = '';
        n++;
      });

      $('.change-currency').on('click', function(e){
        usd = localStorage.getItem('currency');
        symbol = localStorage.getItem('currency_symbol');
        if (symbol == 'USD') {
          localStorage.setItem('currency_symbol','MXN');
          $('.t-symbol').html('MXN');
          coin = ( $('.t-price-1').attr('val') / usd )
        } else {
          localStorage.setItem('currency_symbol','USD');
          $('.t-symbol').html('USD');
          coin = ( $('.t-price-1').attr('val') * usd );
        }
        $('.t-price-1').attr('val',coin);
        $('.t-price-1').html(coin.toFixed(2));
      });

      $('.planes-pago').on('click', function(e) {
        symbol = localStorage.getItem('currency_symbol');
        $.post('php/planes/planes-pago-mp.php', { coin: coin, symbol: symbol }, function( data ) {
          var frame = '<iframe id="planes-frame" src="' + data + '"></iframe">';
          $('#planes-info').html(frame);
        });
      })

    },
    error: function(xhr, tst, err) {
      alert('El tiempo de espera fue superado, por favor intentalo en un momento mas');
      closeall();
    },
    timeout: 10000
  });
}
