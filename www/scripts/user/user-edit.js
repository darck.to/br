$(document).ready(function() {

  carga_user_info();

  //EDITAMOS EL USUARIO
  $('#editUser').submit(function(event) {
    event.preventDefault();

    var key = localStorage.getItem("aUth_key");
    var user = localStorage.getItem("aUth_user");

    var formData = new FormData(document.getElementById("editUser"));
    formData.append("key", key);
    formData.append("user", user);
    console.log(key, user);
    var formMethod = $(this).attr('method');
    var rutaScrtip = $(this).attr('action');

    var request = $.ajax({
      url: rutaScrtip,
      method: formMethod,
      data: formData,
      contentType: false,
      processData: false,
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
    });
    // handle the responses
    request.done(function(data) {
      carga_info_perfil();
    })
    request.fail(function(jqXHR, textStatus) {
      console.log(textStatus);
    })
    request.always(function(data) {
      // clear the form
      $('#editUser').trigger('reset');
      $('.modal').toggleClass("is-active");
    });

  });

  translate();

});

function carga_user_info(e) {
  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  $.ajax({
    type: "POST",
    url: "php/user/user-load.php",
    data: {
      auth : stringaUth_key,
      user : stringaUth_user
    },
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $.each(data, function (name, value) {
        $('#i-usu').val(value.usu);
        $('#i-mai').val(value.mai);
        $('#i-nom').val(value.nom);
        $('#i-ape').val(value.ape);
        $('#i-mat').val(value.mat);
        $('#i-tel').val(value.tel);
        $('#i-cel').val(value.cel);
        //$('#i-error').html(value.error);
      });
    },
    error: function(xhr, tst, err) {
      alert(translate_string('error_ajax'));
      closeall();
    },
    timeout: 10000
  })

}

//EMAIL
$('#i-mai').on('keyup', function() {
	email = $(this).val();
	if (validateEmail(email)) {
    $('.editButton').prop('disabled',false);
	} else {
    $('.editButton').prop('disabled',true);
	}

  if (this.value.length == 0) {
    $('.editButton').prop('disabled',true);
  }
});

//CONFIMATION
$('#i-pac').on('keyup', function() {
	pass = $('#i-pas').val();
	confirm = $(this).val();
	if (confirm != pass) {
    $('.editButton').prop('disabled',true);
	} else {
    $('.editButton').prop('disabled',false);
	}
});

function validateEmail(sEmail) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {
		return true;
	}
	else {
		return false;
	}
}
