$(document).ready(function() {

  $('.exitFsOverlay').on('click', function(e){
    $('.overlay-full').html('');
  })

  carga_products_map()

  translate()

  $('.changeCurrency').on('click', function(e) {
    var txt = $('.product-symbol').html();
    console.log(txt);
    if (txt == 'MXN') {
      localStorage.setItem('currency_symbol','USD');
      $('.product-symbol').html('USD')
    } else if (txt == 'USD') {
      localStorage.setItem('currency_symbol','MXN');
      $('.product-symbol').html('MXN')
    }
  })

});

function carga_products_map(e) {
  //TIPO DE CAMBIO
  var curr = localStorage.getItem('currency_symbol');
  $('.product-symbol').html(curr);

  carga_loader();
  var iconNear = L.icon({
    iconUrl: 'img/leaf/near.png',
    iconSize: [35, 35]
  });

  //COORDENADAS PERSONALES
  navigator.geolocation.getCurrentPosition(function(location) {
    var Nlatlng = new L.LatLng(location.coords.latitude, location.coords.longitude);

    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var pro_index = localStorage.getItem("proshow");
    $.ajax({
      type: "POST",
      url: "php/products/products-show.php",
      data: {
        auth : stringaUth_key,
        user : stringaUth_user,
        pro_index : pro_index
      },
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        $.each(data, function(key,value){
          if (value.success == true) {
            var promap = L.map('productsmap').setView([value.lat, value.lng], 13);
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
              attribution: ''
            }).addTo(promap);
            L.marker([value.lat, value.lng], {icon: iconNear}).addTo(promap).on('click', function(e) {
              L.Routing.control({
                waypoints: [
                  L.latLng(e.latlng),
                  L.latLng(Nlatlng)
                ]
              }).addTo(promap);
            });

            //OPCIONES DESCRIPCION Y FILTERS
            var ulopt = '<ul class="is-size-6 has-text-grey">';
              var listart = '<li>';
              var liend = '</li>' ;
              if (parseInt(value.cuartos) > 0) { $('#p-cua').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_cua') + ':&nbsp;' + value.cuartos + liend; }
              if (value.jacuzzi == 'true') { $('#p-jac').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_jac') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (value.air == 'true') { $('#p-air').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_air') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (value.jardin == 'true') { $('#p-jar').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_jar') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (value.trasero == 'true') { $('#p-tra').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_tra') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (value.chimenea == 'true') { $('#p-chi').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_chi') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (parseInt(value.banos) > 0) { $('#p-ban').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_ban') + ':&nbsp;' + value.banos + liend; }
              if (value.cochera > 1) { var txt = translate_string('products_opc_ves'); } else { var txt = translate_string('products_opc_veh');}
              if (parseInt(value.cochera) > 0) { $('#p-coc').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_coc') + ':&nbsp;' + value.cochera + '&nbsp;' + txt + liend; }
              if (value.piscina == 'true') { $('#p-pis').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_pis') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (value.terraza == 'true') { $('#p-ter').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_ter') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (value.balcon == 'true') { $('#p-bal').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_bal') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (value.seguridad == 'true') { $('#p-seg').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_seg') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (value.recepcion == 'true') { $('#p-rec').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_rec') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (value.gimnasio == 'true') { $('#p-gim').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_gim') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
            ulopt += '</ul>';

            $('#imgHolder').html('');
            for (var i = 0; i < value.multimedia; i++) {
              $('#imgHolder').append('<img class="product-img-showcase show-img" src="assets/pro_img/' + pro_index + '/' + pro_index + '_' + i + '.png" />');
            }

            $('.show-img').on('click', function(e){
              carga_modal();
              $('.modal-content').html('');
              for (var i = 0; i < value.multimedia; i++) {
                $('.modal-content').append('<img src="assets/pro_img/' + pro_index + '/' + pro_index + '_' + i + '.png" />');
              }
            })

            var imgusu = 'assets/perf_img/' + value.perf_index + '.png';

            var renta = translate_string('products_input_ven');
            if(value.ren == 1) {renta = translate_string('products_input_ren');}
            $('#p-cat').html(value.cat);
            $('#p-ren').html(renta);
            $('#p-tit').html(value.nom);
            $('#p-tel').html(value.tel);
            $('#p-cel').html(value.cel);
            $('#p-mai').html(value.mai);
            $('#img-usu').html('<img src=' + imgusu + '>');
            $('#p-usu').html(value.usu);
            $('#p-tel').html(value.tel);
            $('#p-cel').html(value.cel);
            $('#p-des').html(value.des);
            $('#p-cal').html(value.cal);
            $('#p-col').html(value.col);
            $('#p-ciu').html(value.ciu + ', cp ' + value.cp);
            $('#p-pai').html(value.est + ', ' + value.pai);
            $('#p-opc').html(ulopt);
            $('#p-des').html(value.des);
            var precio = money_format(value.pre);
            $('#p-pre').html(precio + '<i class="has-background-grey-light is-size-7 has-text-white bo-r-five ma-half pa-half">' + value.cur + '</i>');

            $('.filter-options').on('click', function(e) {
              var who = $(this).children('i');
              $.ajax({
                url: "assets/opc_br/opc_br.json",
                cache: false,
                success: function(results) {
                  $.each(results, function(index,content){
                    if (who.hasClass(content.icon)) {
                      toast(content.name)
                    }
                  })
                },
                error: function(xhr, tst, err) {
                  alert(translate_string('error_ajax'));
                }
              })
            })

            $('#img-usu img').on('click', function(e){
              carga_modal();
              $('.modal-content').html('<img src="' + $(this).attr('src') + '" />');
            })

            //AGREGAMOS MODAL PARA CONTACTO A button
            $('#send-message').addClass('button-caller');

            $('.button-caller').on('click', function(e){
              carga_modal();
              var content = '<div class="box has-background-white bo-r-five">'
                content += '<div class="columns is-vcentered">';
                  content += '<h2 class="is-size-4 has-text-centered fo-w-l pa-one pa-no-b">' + translate_string('products_h2_sen') + '</h2>';
                  content += '<h3 class="is-size-6 has-text-centered fo-w-l pa-b-one">' + translate_string('products_h3_sel') + '</h3>';
                  content += '<div class="columns is-multiline is-mobile has-text-centered">';
                    content += '<div class="column is-3 has-text-centered">';
                      content += '<a class="pa-one" href="https://wa.me/52' + value.cel + '/?text=Hola, me puedes dar mas informacion de la propiedad que tienes en ' + renta + ': ' + value.nom +  '; que tienes publicada en la app Bienes Raices Tijuana"><i class="fab fa-3x fa-whatsapp has-text-primary"></i></a>';
                      content += '<h6 class="is-size-7 has-text-centered fo-w-b" data-translate="products_h6_wha">Whatsapp</h6>';
                    content += '</div>';
                    content += '<div class="column is-3 has-text-centered">';
                      content += '<a class="pa-one" onclick=\"window.plugins.socialsharing.shareViaSMS(\'Hola, me puedes dar mas informacion de la propiedad que tienes en ' + renta + ': ' + value.nom +  '; que tienes publicada en la app Bienes Raices Tijuana\', ' + value.cel + ', function(msg) {console.log(\'ok: \' + msg)}, function(msg) {alert(\'error: \' + msg)} )\"><i class="fas fa-3x fa-sms has-text-info"></i></a>';
                      content += '<h6 class="is-size-7 has-text-centered fo-w-b" data-translate="products_h6_sms">SMS</h6>';
                    content += '</div>';
                    content += '<div class="column is-3 has-text-centered">';
                      if (value.cel) {celular = '<a class="pa-one" href="tel:' + value.cel + '"><i class="fas fa-3x fa-mobile-alt has-text-link"></i></a>'; } else {celular = '';}
                      content += celular;
                      content += '<h6 class="is-size-7 has-text-centered fo-w-b" data-translate="products_h6_cel">Cellphone</h6>';
                    content += '</div>';
                    content += '<div class="column is-3 has-text-centered">';
                      if (value.tel) {fijo = '<a class="pa-one" href="tel:' + value.tel + '"><i class="fas fa-3x fa-phone has-text-success"></i></a>'; } else {fijo = '';}
                      content += fijo;
                      content += '<h6 class="is-size-7 has-text-centered fo-w-b" data-translate="products_h6_ofi">Office</h6>';
                    content += '</div>';
                  content += '</div>';
                content += '</div>';
              content += '</div>';
              $('.modal-content').html(content);

            });

          }
        })
        cierra_loader();
      },
      error: function(xhr, tst, err) {
        alert(translate_string('error_ajax'));
        closeall();
      },
      timeout: 10000
    });

  })

}
