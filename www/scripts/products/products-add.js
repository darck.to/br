$(document).ready(function() {
var opc, mult, inum;

  //NAVIGATOR
  $('.exitFsOverlay').on('click', function(e){
    $('.overlay-full').html('');
  });
  //TAB
  $('.tab-option').on('click', function(e){
    var option = $(this).attr('opt');
    $('.tab').addClass('is-hidden');
    $('.tab-'+option).toggleClass('is-hidden');
    $('.tab-option').removeClass('is-active');
    $(this).addClass('is-active');
  });

  carga_plan();
  carga_products_map();
  carga_categorias();

  function carga_plan(e) {
    carga_loader();
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    $.ajax({
      type: "POST",
      url: "php/user/user-plan.php",
      data: {
        auth : stringaUth_key,
        user : stringaUth_user
      },
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        var pro_index;
        var img_edit = 'img/stock.png';
        $('#multimediaCajas').html('');
        $.each(data, function (name, value) {
          $.getJSON('assets/plus_br/paq_' + value.plan + '.json',function(data){
            $.each(data, function(key,val){
              mult = val.mult;
              opc = val.opc;
              carga_opciones(opc);
              $('#opcionesNo').html(opc);
              $('#multimediaNo').html(mult);
              for (i = 0; i < mult; i++) {
                imgc = '<div class="column is-half">';
                  imgc += '<div class="columns is-mobile is-multiline has-text-centered pa-no ma-no">';
                    imgc += '<div class="column is-full has-text-centered is-relative product-container pa-no">';
                      //SI ES EDICION
                      if (localStorage.getItem("edit_id") != null) {
                        pro_index = localStorage.getItem("edit_id");
                        img_edit = 'assets/pro_img/' + pro_index + '/' + pro_index + '_' + i + '.png';
                      }
                      imgc += '<img src="' + img_edit + '" id="imgContainer' + i + '" class="product-img-container">';
                    imgc += '</div>';
                    imgc += '<div class="column is-half has-text-centered has-background-grey ma-no but_take" val=' + i + '>';
                      imgc += '<i class="has-text-white fas fa-2x fa-camera"></i>';
                    imgc += '</div>';
                    imgc += '<div class="column is-half has-text-centered has-background-grey ma-no but_select" val=' + i + '>';
                      imgc += '<i class="has-text-white far fa-2x fa-image"></i>';
                    imgc += '</div>';
                  imgc += '</div>';
                imgc += '</div>';
                $('#multimediaCajas').append(imgc);
              };
            });
            //IMAGENES ROTAS
            $('.product-img-container').on('error', function () {
                $(this).attr('src', 'img/stock.png');
            });

            // Take photo from camera
            $('.but_take').click(function(){
              inum = $(this).attr('val');
              navigator.camera.getPicture(onSuccess, onFail, { quality: 90,
                correctOrientation: true,
                targetWidth: 900,
                targetHeight: 900,
                destinationType: Camera.DestinationType.FILE_URL
              });
            });

            // Select from gallery
            $(".but_select").click(function(){
              inum = $(this).attr('val');
              navigator.camera.getPicture(onSuccess, onFail, { quality: 90,
                targetWidth: 900,
                targetHeight: 900,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: true,
                destinationType: Camera.DestinationType.FILE_URI
              });
            });

            // Change image source
            function onSuccess(imageData) {
              var url = "php/products/products-preimg.php";
              var image = document.getElementById('imgContainer' + inum);
              image.src = imageData + '?' + Math.random();

              carga_loader();
              var options = new FileUploadOptions();
              options.fileKey = "file";
              options.fileName = imageData.substr(imageData.lastIndexOf('/') + 1);
              options.mimeType = "image/jpeg";

              var params = {};
              params.auth = localStorage.getItem("aUth_key");
              params.user = localStorage.getItem("aUth_user");
              //SI ES EDICION
              if (localStorage.getItem("edit_id") != null) {
                params.id = localStorage.getItem("edit_id");
                url = "php/products/products-editimg.php";
              }
              params.num = inum;

              options.params = params;
              options.chunkedMode = false;

              var ft = new FileTransfer();
              ft.upload(imageData, url, function(result){
                $.each(result,function(index,content){
                  if (content.success == true) {
                    toast(content.img)
                  }
                })
                cierra_loader();
              }, function(error){
                cierra_loader();
                alert('error : ' + JSON.stringify(error));
              }, options);
            }

            function onFail(message) {
              //alert('No seleccionaste imagen, error: ' + message);
            }

          });
        });
        cierra_loader();
      },
      error: function(xhr, tst, err) {
        alert(translate_string('error_ajax'));
        closeall();
      },
      timeout: 10000
    });

    function carga_opciones(counter) {
      $.getJSON('assets/opc_br/opc_br.json',function(data){
        $('#product-opc').html('');
        var opc = "";
        $.each(data, function(id,content){
          opc += '<div class="column is-6 is-relative pa-no">';
            opc += '<div class="filter-options opt-' + content.id + '">';
              opc += '<i class="has-text-white ' + content.icon + '"></i>';
            opc += '</div>';
            if (content.type == "integer") {input = '<input id="num-' + content.id +'" class="input integer-input" type="number" value=1>';} else { input = "";}
            opc += '<span class="is-hidden"><small class="has-text-white-ter">' + content.name + '</small>' + input + '</span>';
          opc += '</div>';
        })
        $('#product-opc').append(opc);
        //OPCIONES
        $('.filter-options').on('click', function(e){
          var count = $('.count').length;
          if (count == counter) {
            if ($(this).hasClass('count')) {
              $(this).toggleClass('has-background-primary count');
              $(this).siblings('span').toggleClass('is-hidden');
              $(this).parent().toggleClass('has-background-primary');
              $(this).parent().toggleClass('shadow-one');
            }
          } else {
            //SELECCIONA LA OPCION
            $(this).toggleClass('has-background-primary count');
            $(this).siblings('span').toggleClass('is-hidden');
            $(this).parent().toggleClass('has-background-primary');
            $(this).parent().toggleClass('shadow-one');
          }
        });

      })
      //SI ES EDICION
      var stringaUth_key = localStorage.getItem("aUth_key");
      var stringaUth_user = localStorage.getItem("aUth_user");
      var pro_index = localStorage.getItem("edit_id");
      $.ajax({
        type: "POST",
        url: "php/products/products-show.php",
        data: {
          auth : stringaUth_key,
          user : stringaUth_user,
          pro_index : pro_index
        },
        async:true,
        dataType : 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
        success: function(data) {
          $.each(data, function(key,value){
            if (value.success == true) {
              if (parseInt(value.cuartos) > 0) {
                $('.opt-cuartos').toggleClass('has-background-primary count');
                $('.opt-cuartos').siblings('span').toggleClass('is-hidden');
                $('.opt-cuartos').parent().toggleClass('has-background-primary');
                $('.opt-cuartos').parent().toggleClass('shadow-one');
                $('#num-cuartos').val(value.cuartos);
              }
              if (value.jacuzzi == 'true') {
                $('.opt-jacuzzi').toggleClass('has-background-primary count');
                $('.opt-jacuzzi').siblings('span').toggleClass('is-hidden');
                $('.opt-jacuzzi').parent().toggleClass('has-background-primary');
                $('.opt-jacuzzi').parent().toggleClass('shadow-one');
              }
              if (value.air == 'true') {
                $('.opt-air').toggleClass('has-background-primary count');
                $('.opt-air').siblings('span').toggleClass('is-hidden');
                $('.opt-air').parent().toggleClass('has-background-primary');
                $('.opt-air').parent().toggleClass('shadow-one');
              }
              if (value.jardin == 'true') {
                $('.opt-jardin').toggleClass('has-background-primary count');
                $('.opt-jardin').siblings('span').toggleClass('is-hidden');
                $('.opt-jardin').parent().toggleClass('has-background-primary');
                $('.opt-jardin').parent().toggleClass('shadow-one');
              }
              if (value.trasero == 'true') {
                $('.opt-trasero').toggleClass('has-background-primary count');
                $('.opt-trasero').siblings('span').toggleClass('is-hidden');
                $('.opt-trasero').parent().toggleClass('has-background-primary');
                $('.opt-trasero').parent().toggleClass('shadow-one');
              }
              if (value.chimenea == 'true') {
                $('.opt-chimenea').toggleClass('has-background-primary count');
                $('.opt-chimenea').siblings('span').toggleClass('is-hidden');
                $('.opt-chimenea').parent().toggleClass('has-background-primary');
                $('.opt-chimenea').parent().toggleClass('shadow-one');
              }
              if (parseInt(value.banos) > 0) {
                $('.opt-banos').toggleClass('has-background-primary count');
                $('.opt-banos').siblings('span').toggleClass('is-hidden');
                $('.opt-banos').parent().toggleClass('has-background-primary');
                $('.opt-banos').parent().toggleClass('shadow-one');
                $('#num-' + value.key).val(value.banos);
              }
              if (parseInt(value.cochera) > 0) {
                $('.opt-chochera').toggleClass('has-background-primary count');
                $('.opt-chochera').siblings('span').toggleClass('is-hidden');
                $('.opt-chochera').parent().toggleClass('has-background-primary');
                $('.opt-chochera').parent().toggleClass('shadow-one');
                $('#num-cochera').val(value.cochera);
              }
              if (value.piscina == 'true') {
                $('.opt-piscina').toggleClass('has-background-primary count');
                $('.opt-piscina').siblings('span').toggleClass('is-hidden');
                $('.opt-piscina').parent().toggleClass('has-background-primary');
                $('.opt-piscina').parent().toggleClass('shadow-one');
              }
              if (value.terraza == 'true') {
                $('.opt-terraza').toggleClass('has-background-primary count');
                $('.opt-terraza').siblings('span').toggleClass('is-hidden');
                $('.opt-terraza').parent().toggleClass('has-background-primary');
                $('.opt-terraza').parent().toggleClass('shadow-one');
              }
              if (value.balcon == 'true') {
                $('.opt-balcon').toggleClass('has-background-primary count');
                $('.opt-balcon').siblings('span').toggleClass('is-hidden');
                $('.opt-balcon').parent().toggleClass('has-background-primary');
                $('.opt-balcon').parent().toggleClass('shadow-one');
              }
              if (value.seguridad == 'true') {
                $('.opt-seguridad').toggleClass('has-background-primary count');
                $('.opt-seguridad').siblings('span').toggleClass('is-hidden');
                $('.opt-seguridad').parent().toggleClass('has-background-primary');
                $('.opt-seguridad').parent().toggleClass('shadow-one');
              }
              if (value.recepcion == 'true') {
                $('.opt-recepcion').toggleClass('has-background-primary count');
                $('.opt-recepcion').siblings('span').toggleClass('is-hidden');
                $('.opt-recepcion').parent().toggleClass('has-background-primary');
                $('.opt-recepcion').parent().toggleClass('shadow-one');
              }
              if (value.gimnasio == 'true') {
                $('.opt-gimnasio').toggleClass('has-background-primary count');
                $('.opt-gimnasio').siblings('span').toggleClass('is-hidden');
                $('.opt-gimnasio').parent().toggleClass('has-background-primary');
                $('.opt-gimnasio').parent().toggleClass('shadow-one');
              }
              $('#i-tit').val(value.nom);
              $('#i-lat').val(value.lat);
              $('#i-lng').val(value.lng);
              $('#i-cal').val(value.cal);
              $('#i-num').val(value.num);
              $('#i-col').val(value.col);
              $('#i-ciu').val(value.ciu);
              $('#i-cp').val(value.cp);
              $('#i-est').val(value.est);
              $('#i-pai').val(value.pai);
              $('#i-des').html(value.des);
              if(value.ren == 1) {$('#i-ren').prop( "checked", true );} else {$('#i-ven').prop( "checked", true );}
              $('#i-pre').val(value.pre);
              $('#i-cat').prepend('<option value=' + value.cate_index + ' selected>' + value.cat + '</option>');
              //TEXTO DE EDICION IN header
              $('#editTxt').html(translate_string('products_edit_press'));
              //BOTON DE ELIMINAR
              $('.publish-button').html('Editar');
              var del = '<div class="columns is-12 has-text-centered pa-one width-100">';
                del += '<button class="button has-background-danger has-text-white input-button-rounded input-button-small delete-button" onclick=elimina_producto("' + pro_index + '")>' + translate_string('products_edit_del') + '</button>'
              del += '</div>';
              $('#delButton').html(del);
            }
          })
          cierra_loader();
        },
        error: function(xhr, tst, err) {
          alert(translate_string('error_ajax'));
          closeall();
        },
        timeout: 10000
      });
    }

  }

  function carga_categorias(e) {
    //CURRENCY
    $('.product-symbol').html(localStorage.getItem('currency_symbol'));
    $('#categorias-select').toggleClass('is-loading');
    $.ajax({
      type: "POST",
      url: "php/products/products-categorias.php",
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        $('#i-cat').html('');
        $.each(data, function (name, value) {
          $('#i-cat').append('<option value=' + value.cate_index + '>' + value.nom + '</option>');
        });
        $('#i-cat').toggleClass('is-loading');
      },
      error: function(xhr, tst, err) {
        alert(translate_string('error_ajax'));
        closeall();
      },
      timeout: 10000
    })
  }

  translate();

})

function carga_products_map(e) {

  var promap = L.map('productsmap').setZoom(13);

  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: ''
  }).addTo(promap);

  function onLocationFoundProduct(e) {

    var geocodeService = L.esri.Geocoding.geocodeService();
    var marker;

    promap.on('click', function (e) {
      geocodeService.reverse().latlng(e.latlng).run(function (error, result) {
        if (error) {
          return;
        }
        if (marker) { // check
            promap.removeLayer(marker); // remove
        }
        marker = L.marker(result.latlng).addTo(promap).bindPopup(result.address.Match_addr).openPopup();
        $('#i-cal').val(result.address.Address);
        $('#i-num').val(result.address.AddNum);
        $('#i-col').val(result.address.Neighborhood);
        $('#i-ciu').val(result.address.Subregion);
        $('#i-est').val(result.address.Region);
        $('#i-pai').val(result.address.CountryCode);
        $('#i-cp').val(result.address.Postal);
        $('#i-lat').val(e.latlng.lat);
        $('#i-lng').val(e.latlng.lng);
      });
    });

  }

  function onLocationErrorProduct(e) {
    alert(e.message);
  }

  promap.on('locationfound', onLocationFoundProduct);
  promap.on('locationerror', onLocationErrorProduct);

  promap.locate({setView: true, maxZoom: 16});
}

$(".integer-input").on("keypress keyup blur",function (event) {
   $(this).val($(this).val().replace(/[^\d].+/, ""));
    if ((event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
});

function guarda_producto_bdd(e) {
  var mod = false;
  var list = '<div class="has-background-white ma-half pa-one bo-r-five">';
  list += '<h1 class="fo-w-l has-text-centered has-text-danger">Campos vacios:</h1>';
  list += '<ul>';

  var tit = $('#i-tit').val();
    if (!tit) {list += '<li>Titulo</li>'; mod = true};
  var lat = $('#i-lat').val();
    if (!lat) {list += '<li>Latitud</li>'; mod = true};
  var lng = $('#i-lng').val();
    if (!lng) {list += '<li>Longitud</li>'; mod = true};
  var cal = $('#i-cal').val();
    if (!cal) {list += '<li>Calle</li>'; mod = true};
  var num = $('#i-num').val();
    if (!num) {list += '<li>Numero</li>'; mod = true};
  var col = $('#i-col').val();
    if (!col) {list += '<li>Colonia/Comunidad</li>'; mod = true};
  var ciu = $('#i-ciu').val();
    if (!ciu) {list += '<li>Ciudad/Region</li>'; mod = true};
  var est = $('#i-est').val();
    if (!est) {list += '<li>Estado/Provincia</li>'; mod = true};
  var pai = $('#i-pai').val();
    if (!pai) {list += '<li>Pais</li>'; mod = true};
  var cp = $('#i-cp').val();
    if (!cp) {list += '<li>CP</li>'; mod = true};
  var des = $('#i-des').val();
    if (!des) {list += '<li>Descripcion</li>'; mod = true};
  var ren = $('input[name="ren"]:checked').val();
    if (!ren) {list += '<li>Renta o Venta</li>'; mod = true};
  var pre = $('#i-pre').val();
    if (!pre) {list += '<li>Precio</li>'; mod = true};
  var cat = $('#i-cat').val();
    if (!cat) {list += '<li>Categorias</li>'; mod = true};

  if ($('.count').length == 0) {list += '<li>No hay opciones seleccionadas</li>'; mod = true};

  if ($('.opt-cuartos').hasClass('count')) { var cuartos = $('#num-cuartos').val(); } else { var cuartos = 0; }
  if ($('.opt-jacuzzi').hasClass('count')) { var jacuzzi = true; } else { var jacuzzi = false; }
  if ($('.opt-air').hasClass('count')) { var air = true; } else { var air = false; }
  if ($('.opt-jardin').hasClass('count')) { var jardin = true; } else { var jardin = false; }
  if ($('.opt-trasero').hasClass('count')) { var trasero = true; } else { var trasero = false; }
  if ($('.opt-chimenea').hasClass('count')) { var chimenea = true; } else { var chimenea = false; }
  if ($('.opt-banos').hasClass('count')) { var banos = $('#num-banos').val(); } else { var banos = 0; }
  if ($('.opt-cochera').hasClass('count')) { var cochera = $('#num-cochera').val(); } else { var cochera = 0; }
  if ($('.opt-piscina').hasClass('count')) { var piscina = true; } else { var piscina = false; }
  if ($('.opt-terraza').hasClass('count')) { var terraza = true; } else { var terraza = false; }
  if ($('.opt-balcon').hasClass('count')) { var balcon = true; } else { var balcon = false; }
  if ($('.opt-seguridad').hasClass('count')) { var seguridad = true; } else { var seguridad = false; }
  if ($('.opt-recepcion').hasClass('count')) { var recepcion = true; } else { var recepcion = false; }
  if ($('.opt-gimnasio').hasClass('count')) { var gimnasio = true; } else { var gimnasio = false; }

  list += '</ul>';
  list += '</div>';

  if (mod) {
    carga_modal( );
    $('.modal-content').html(list);
    return false;
  }

  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  var cur = localStorage.getItem('currency_symbol');
  var mult = $('#multimediaNo').html();
  var ruta = "php/products/products-add.php";
  var pro_index = null;
  //SI ES EDICION
  if (localStorage.getItem("edit_id") != null) {
    ruta = "php/products/products-edit.php";
    pro_index = localStorage.getItem("edit_id");
  }
  $.ajax({
    type: "POST",
    url: ruta,
    data: {
      auth : stringaUth_key,
      user : stringaUth_user,
      pro_index: pro_index,
      tit : tit,
      lat : lat,
      lng : lng,
      cal : cal,
      num : num,
      col : col,
      ciu : ciu,
      est : est,
      pai : pai,
      cp : cp,
      des : des,
      ren : ren,
      cur : cur,
      pre : pre,
      cat : cat,
      cuartos : cuartos,
      jacuzzi : jacuzzi,
      air : air,
      jardin : jardin,
      trasero : trasero,
      chimenea : chimenea,
      banos : banos,
      cochera : cochera,
      piscina : piscina,
      terraza : terraza,
      balcon : balcon,
      seguridad : seguridad,
      recepcion : recepcion,
      gimnasio : gimnasio,
      mult : mult
    },
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      var list = '<div class="has-background-white ma-half pa-one bo-r-five">';
      $.each(data, function (name, value) {
        if(value.success) {
          //ELIMINAMOS localStorage DE EDICION
          localStorage.removeItem('edit_id');
          list += '<h1 class="has-text-centered ma-one is-size-1 fo-w-l">' + value.info + '</h1>';
          list += '<h1 class="has-text-centered pa-one"><i class="has-text-success far fa-4x fa-smile"></i></h1>';
        } else {
          list += '<h1 class="has-text-centered ma-one is-size-1 fo-w-l">' + value.error + '</h1>';
          list += '<h1 class="has-text-centered pa-one"><i class="has-text-danger far fa-4x fa-frown-open"></i></h1>';
        }
      });
      list += '</div>';
      carga_modal();
      $('.modal-content').html(list);
      $('.overlay-principal').html('');
      $('.overlay-full').html('');
      carga_main_body();
    },
    error: function(xhr, tst, err) {
      alert('El tiempo de espera fue superado, por favor intentalo en un momento mas');
      closeall();
    },
    timeout: 10000
  })
}

function elimina_producto(e) {
  var confirmation = confirm(translate_string('products_confirm_del'));
  if (confirmation) {
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var pro_index = e;
    $.ajax({
      type: "POST",
      url: "php/products/products-del.php",
      data: {
        auth : stringaUth_key,
        user : stringaUth_user,
        pro_index : pro_index
      },
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        var list = '<div class="has-background-white ma-half pa-one bo-r-five">';
        $.each(data, function (name, value) {
          if(value.success) {
            list += '<h1 class="has-text-centered ma-one is-size-1 fo-w-l">' + value.info + '</h1>';
            list += '<h1 class="has-text-centered pa-one"><i class="has-text-success far fa-4x fa-smile"></i></h1>';
          } else {
            list += '<h1 class="has-text-centered ma-one is-size-1 fo-w-l">' + value.error + '</h1>';
            list += '<h1 class="has-text-centered pa-one"><i class="has-text-danger far fa-4x fa-frown-open"></i></h1>';
          }
        });
        list += '</div>';
        carga_modal();
        $('.modal-content').html(list);
        $('.overlay-principal').html('');
        $('.overlay-full').html('');
      },
      error: function(xhr, tst, err) {
        alert('El tiempo de espera fue superado, por favor intentalo en un momento mas');
        closeall();
      },
      timeout: 10000
    })
  }
}
