$(document).ready(function() {

  $('.exitFsOverlay').on('click', function(e){
    $('.overlay-sub').html('');
  });

  carga_categorias();

  translate();

});

function carga_categorias(e) {
  $.ajax({
    type: "POST",
    url: "php/products/products-categorias.php",
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $('#categorias-scroll').html('');
      var content = '<div class="column is-12 pa-two"></div>';
      var number = 0;
      $.each(data, function (name, value) {
        content += '<div class="column is-6" onclick=carga_publicaciones_cat("' + value.cate_index + '")>';
        content += '<div class="card categoria-show" val=' + value.cate_index + '>';
        content += '<div class="card-image">';
        content += '<figure class="image is-4by3 is-relative">';
        content += '<img class="categoria-img" src="' + value.img + '" alt="' + value.nom + '">';
        content += '</figure>';
        content += '</div>';
        content += '<div class="card-content">';
        content += '<div class="media-content">';
        content += '<p class="is-size-6 fo-w-l">' + value.nom + '</p>';
        if (value.num == 1) { txt = translate_string('products_cat_pub'); } else { txt = translate_string('products_cat_pus'); }
        content += '<p class="is-size-7 has-text-grey">' + value.num + '&nbsp;' + txt + '</p>';
        content += '</div>';
        content += '</div>';
        content += '</div>';
        content += '</div>';
      });
      $('#categorias-scroll').html(content);
    },
    error: function(xhr, tst, err) {
      alert(translate_string('error_ajax'));
      closeall();
    },
    timeout: 10000
  })
}

function carga_publicaciones_cat(e) {
  var cate_index = e;
  $('.overlay-sub').html('');
  $('.overlay-principal').html('');
  $(".navbar-menu").toggleClass("is-active");
  localStorage.setItem('cate_index',cate_index);
  $('.overlay-sub').load('templates/search/search.html');
}
