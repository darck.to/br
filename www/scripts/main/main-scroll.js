$(document).ready(function() {

  carga_main_scroll();
  translate();

});

function carga_main_scroll(e) {

  function onLocationFound(e) {
    carga_loader();
    var latNow = localStorage.getItem("lat");
    var lngNow = localStorage.getItem("lng");
    //BUSCA COINCIDENCIAS EN 50KLM A LA REDONDA
    var dis = localStorage.getItem("range");
    dis = dis * 1.609344; //KLM
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    $.ajax({
      type: "POST",
      url: "php/main/main-scroll.php",
      data: {
        auth : stringaUth_key,
        user : stringaUth_user,
        lat : latNow,
        lng : lngNow,
        dis : dis
      },
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        var box = "";
        box += '<div class="box ma-one"></div>';
        $.each(data, function (name, value) {
          if (value.success) {
            box += '<div class="box ma-b-one ma-lr-one pa-no product-show" id=' + value.pro_index + '>';
              box += '<div class="columms ma-no pa-no">';
                box += '<div class="columm ma-no pa-no">';
                  box += '<img class="main-scroll-overlay-img" src="assets/pro_img/' + value.pro_index  + '/' + value.pro_index  + '_0.png"/>';
                box += '</div>';
                box += '<div class="columm pa-one">';
                  box += '<div class="content">';
                    box += '<h1 class="is-size-4 fo-w-l">' + value.nom + '</h1>';
                    box += '<div class="columns ma-no-b pa-no-b">';
                      box += '<div class="column has-text-centered">';
                        if (parseInt(value.cuartos) > 0) { bg = 'has-background-grey-dark';} else {bg = '';}
                        box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                          box += '<i class="has-text-white fas fa-cube"></i>';
                        box += '</div>';
                        if (value.jacuzzi == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                        box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                          box += '<i class="has-text-white fas fa-hot-tub"></i>';
                        box += '</div>';
                        if (value.air == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                        box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                          box += '<i class="has-text-white fas fa-wind"></i>';
                        box += '</div>';
                        if (value.jardin == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                        box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                          box += '<i class="has-text-white fas fa-tree"></i>';
                        box += '</div>';
                        if (value.trasero == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                        box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                          box += '<i class="has-text-white fas fa-bath"></i>';
                        box += '</div>';
                        if (value.chimenea == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                        box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                          box += '<i class="has-text-white fas fa-fire-alt"></i>';
                        box += '</div>';
                        if (parseInt(value.banos) > 0) { bg = 'has-background-grey-dark';} else {bg = '';}
                        box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                          box += '<i class="has-text-white fas fa-lock"></i>';
                        box += '</div>';
                        if (parseInt(value.cochera) > 0) { bg = 'has-background-grey-dark';} else {bg = '';}
                        box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                          box += '<i class="has-text-white fas fa-car-side"></i>';
                        box += '</div>';
                        if (value.piscina == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                        box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                          box += '<i class="has-text-white fas fa-swimmer"></i>';
                        box += '</div>';
                        if (value.terraza == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                        box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                          box += '<i class="has-text-white fab fa-microsoft"></i>';
                        box += '</div>';
                        if (value.balcon == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                        box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                          box += '<i class="has-text-white fas fa-person-booth"></i>';
                        box += '</div>';
                        if (value.seguridad == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                        box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                          box += '<i class="has-text-white fas fa-user-shield"></i>';
                        box += '</div>';
                        if (value.recepcion == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                        box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                          box += '<i class="has-text-white fas fa-concierge-bell"></i>';
                        box += '</div>';
                        if (value.gimnasio == 'true') { bg = 'has-background-grey-dark';} else {bg = '';}
                        box += '<div class="filter-options ma-2 pa-half ' + bg + '">';
                          box += '<i class="has-text-white fas fa-dumbbell"></i>';
                        box += '</div>';
                      box += '</div>';
                    box += '</div>';
                  box += '</div>';
                box += '</div>';
              box += '</div>';
            box += '</div>';
          }
        });
        box += '<div class="box ma-one"></div>';
        $('#mainScroll').html(box);

        //LAMENTO ESTAS LINEAS PERO EN ANDROID SE SOBREPONIA EL MAPA Y ESTA FUE LA UNICA SOLUCION
        $('.overlay-scroll').css({'cssText': 'z-index: 992 !important'});
        $('.overlay-scroll').css({'cssText': 'z-index: 991 !important'});
        $('.overlay-scroll').css({'cssText': 'z-index: 993 !important'});

        $('.product-show').on('click', function(e){
          $('.overlay-principal').html('');
          var pro_index = $(this).attr('id');
          localStorage.setItem('proshow',pro_index);
          $('.overlay-full').load('templates/products/products-show.html');
        })
        cierra_loader();
      },
      error: function(xhr, tst, err) {
        alert('El tiempo de espera fue superado, por favor intentalo en un momento mas');
        closeall();
      },
      timeout: 10000
    })

  }

  onLocationFound();

}
