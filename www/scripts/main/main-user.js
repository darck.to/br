$(document).ready(function() {

  carga_info_perfil();

  // Take photo from camera
  $('#but_take').click(function(){
    navigator.camera.getPicture(onSuccess, onFail, {
      quality: 90,
      targetWidth: 900,
      targetHeight: 900,
      destinationType: Camera.DestinationType.FILE_URL
    });
  });

  // Select from gallery
  $("#but_select").click(function(){
    navigator.camera.getPicture(onSuccess, onFail, {
      quality: 90,
      targetWidth: 900,
      targetHeight: 900,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      destinationType: Camera.DestinationType.FILE_URI
    });
  });

  // Change image source
  function onSuccess(imageURI) {
    carga_loader();
    var image = document.getElementById('img-avatar');
    image.src = imageURI + '?' + Math.random();

    var options = new FileUploadOptions();
    options.fileKey = "file";
    options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
    options.mimeType = "image/jpeg";

    var params = {};
    params.auth = localStorage.getItem("aUth_key");
    params.user = localStorage.getItem("aUth_user");

    options.params = params;
    options.chunkedMode = false;

    var ft = new FileTransfer();
    ft.upload(imageURI, "php/main/main-avatar.php", function(result){
      cierra_loader()
    }, function(error){
      cierra_loader();
      alert('error : ' + JSON.stringify(error));
    }, options);
  };

  function onFail(message) {
    alert('Failed because: ' + message);
  };

  translate();

});

function carga_info_perfil(e) {
  //ELIMINAMOS localStorage DE EDICION
  localStorage.removeItem('edit_id');
  carga_loader();
  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  $.ajax({
    type: "POST",
    url: "php/main/main-user.php",
    data: {
      auth : stringaUth_key,
      user : stringaUth_user
    },
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $.each(data, function (name, value) {
        $('#img-avatar').attr('src',value.img);
        $('#p-nom').html(value.nom + ' ' + value.ape + ' ' + value.mat);
        $('#p-mai').html('<i class="fas fa-at has-text-primary is-pulled-left user-info-icon"></i>&nbsp;' + value.mai);
        $('#p-tel').html('<i class="fas fa-tty has-text-primary is-pulled-left user-info-icon"></i>&nbsp;' + value.tel);
        $('#p-cel').html('<i class="fas fa-mobile-alt has-text-primary is-pulled-left user-info-icon"></i>&nbsp;' + value.cel);
        //$('#p-error').html(value.error);
      });

      cierra_loader();

      carga_info_propiedades();
    },
    error: function(xhr, tst, err) {
      alert('El tiempo de espera fue superado, por favor intentalo en un momento mas');
      closeall();
    },
    timeout: 10000
  })
}

function carga_info_propiedades(e) {
  carga_loader();
  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  $.ajax({
    type: "POST",
    url: "php/products/products-user.php",
    data: {
      auth : stringaUth_key,
      user : stringaUth_user
    },
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      var box = "";
      $.each(data, function (name, value) {
        if (value.success) {
          var pro_index = value.pro_index;
          box += '<div class="column is-6 pa-no">';
            box += '<div class="box ma-one pa-no product-show" id=' + pro_index + '>';
              box += '<div class="columms ma-no pa-no">';
                box += '<div class="columm ma-no pa-no">';
                  box += '<img class="main-scroll-overlay-img height-170" src="assets/pro_img/' + value.img + '/' + value.img + '_0.png"/>';
                box += '</div>';
                box += '<div class="columm pa-one">';
                  box += '<div class="content">';
                    box += '<h1 class="is-size-7 fo-w-l ma-no">' + value.nom + '</h1>';
                    box += '<small class="is-pulled-left is-size-7 has-text-grey-light pa-b-half">&nbspid: ' + value.pro_index + '</small>';
                    box += '<p class="has-text-right has-text-grey-light is-size-7 pa-no ma-no" data-translate="main_p_man">manten presionado para editar</p>';
                  box += '</div>';
                box += '</div>';
              box += '</div>';
            box += '</div>';
          box += '</div>';
        }
      });
      box += '<div class="column is-12 pa-one"></div>';
      $('#products-user').html(box);

      $('.product-show').on('click', function(e){
        $('.overlay-principal').html('');
        var pro_index = $(this).attr('id');
        localStorage.setItem('proshow',pro_index);
        $('.overlay-full').load('templates/products/products-show.html');
      })
      cierra_loader();
      //CARGA Filtros
      carga_info_filtros();
      //LONGPRESS PARA PROPIEDADES thx https://stackoverflow.com/users/2070060/aztral-enforcer
      var longpress;
      $(".product-show").on('touchstart' ,function(){
        var pro_index = $(this).attr('id');
        longpress=true;
        setTimeout(function() {
          if (longpress) {
            $('.overlay-full').html('');
            $(".navbar-menu").removeClass("is-active");
            $('.overlay-full').load('templates/products/products-add.html');
            localStorage.setItem('proshow',pro_index);
            localStorage.setItem('edit_id',pro_index);
            //alert("long press works!");
          }
        }, 1000);
      })
      $(".product-show").on('touchend' ,function(){
        longpress=false;
      })
    },
    error: function(xhr, tst, err) {
      alert('El tiempo de espera fue superado, por favor intentalo en un momento mas');
      closeall();
    },
    timeout: 10000
  })
}

function carga_info_filtros(e) {
  carga_loader();
  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  $.ajax({
    type: "POST",
    url: "php/main/main-filter-user.php",
    data: {
      auth : stringaUth_key,
      user : stringaUth_user
    },
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      var box = "";
      $.each(data, function (name, value) {
        if (value.success) {
          num = value.num
          if (value.cuartos > 0) { cuartos = 'check';} else { cuartos = 'times';}
          if (value.jacuzzi == 'true') { jacuzzi = 'check';} else { jacuzzi = 'times';}
          if (value.air == 'true') { air = 'check';} else { air = 'times';}
          if (value.jardin == 'true') { jardin = 'check';} else { jardin = 'times';}
          if (value.chimenea == 'true') { chimenea = 'check';} else { chimenea = 'times';}
          if (value.banos > 0) { banos = 'check';} else { banos = 'times';}
          if (value.cochera > 0) { cochera = 'check';} else { cochera = 'times';}
          if (value.piscina == 'true') { piscina = 'check';} else { piscina = 'times';}
          if (value.terraza == 'true') { terraza = 'check';} else { terraza = 'times';}
          if (value.balcon == 'true') { balcon = 'check';} else { balcon = 'times';}
          if (value.seguridad == 'true') { seguridad = 'check';} else { seguridad = 'times';}
          if (value.recepcion == 'true') { recepcion = 'check';} else { recepcion = 'times';}
          if (value.gimnasio == 'true') { gimnasio = 'check';} else { gimnasio = 'times';}
          if (value.mail == true) { mail = 'has-text-primary';} else { mail = 'has-text-white-ter';}
          box += '<div class="column is-12 pa-lr-one">';
            box += '<div class="columns is-multiline is-mobile is-vcentered pa-no">';
              box += '<div class="column is-8 pa-no">';
                box += '<div class="dropdown is-up is-hoverable">';
                  box += '<div class="dropdown-trigger">';
                    box += '<button class="button" aria-haspopup="true" aria-controls="dropdown-menu">';
                      box += '<span>Filtro ' + num + '</span>';
                      box += '<span class="icon is-small">';
                        box += '<i class="fas fa-angle-down" aria-hidden="true"></i>';
                      box += '</span>';
                    box += '</button>';
                  box += '</div>';
                  box += '<div class="dropdown-menu" id="dropdown-menu" role="menu">';
                    box += '<div class="dropdown-content">';
                      box += '<a href="#" class="dropdown-item"><i class="far fa-' + cuartos + '-circle"></i>&nbsp;' + translate_string('products_opc_cua') + '</a>';
                      box += '<a href="#" class="dropdown-item"><i class="far fa-' + jacuzzi + '-circle"></i>&nbsp;' + translate_string('products_opc_jac') + '</a>';
                      box += '<a href="#" class="dropdown-item"><i class="far fa-' + air + '-circle"></i>&nbsp;' + translate_string('products_opc_air') + '</a>';
                      box += '<a href="#" class="dropdown-item"><i class="far fa-' + jardin + '-circle"></i>&nbsp;' + translate_string('products_opc_jar') + '</a>';
                      box += '<a href="#" class="dropdown-item"><i class="far fa-' + chimenea + '-circle"></i>&nbsp;' + translate_string('products_opc_chi') + '</a>';
                      box += '<a href="#" class="dropdown-item"><i class="far fa-' + banos + '-circle"></i>&nbsp;' + translate_string('products_opc_ban') + '</a>';
                      box += '<a href="#" class="dropdown-item"><i class="far fa-' + cochera + '-circle"></i>&nbsp;' + translate_string('products_opc_coc') + '</a>';
                      box += '<a href="#" class="dropdown-item"><i class="far fa-' + piscina + '-circle"></i>&nbsp;' + translate_string('products_opc_pis') + '</a>';
                      box += '<a href="#" class="dropdown-item"><i class="far fa-' + terraza + '-circle"></i>&nbsp;' + translate_string('products_opc_ter') + '</a>';
                      box += '<a href="#" class="dropdown-item"><i class="far fa-' + balcon + '-circle"></i>&nbsp;' + translate_string('products_opc_bal') + '</a>';
                      box += '<a href="#" class="dropdown-item"><i class="far fa-' + seguridad + '-circle"></i>&nbsp;' + translate_string('products_opc_seg') + '</a>';
                      box += '<a href="#" class="dropdown-item"><i class="far fa-' + recepcion + '-circle"></i>&nbsp;' + translate_string('products_opc_rec') + '</a>';
                      box += '<a href="#" class="dropdown-item"><i class="far fa-' + gimnasio + '-circle"></i>&nbsp;' + translate_string('products_opc_gim') + '</a>';
                      box += '<hr class="dropdown-divider">';
                      box += '<a class="dropdown-item">';
                        box += 'Precio: ' + value.precio;
                      box += '</a>';
                    box += '</div>';
                  box += '</div>';
                box += '</div>';
              box += '</div>';
              box += '<div class="column is-4 pa-no">';
              box += '<i class="far fa-2x fa-trash-alt has-text-danger is-pulled-right pa-lr-half filter-delete" val=' + num + '></i>';
                box += '<i class="far fa-2x fa-envelope ' + mail + ' is-pulled-right pa-lr-half filter-mail"></i>';
              box += '</div>';
            box += '</div>';
          box += '</div>';
        }
      });
      box += '<div class="column is-12 pa-one"></div>';
      $('#filters-user').html(box);
      //BORRA FILTROS
      $('.filter-delete').on('click', function(e){
        var num = $(this).attr('val');
        carga_modal();
        content = '<div class="box has-background-white bo-r-five pa-one has-text-centered">';
          content += '<h2 class="is-size-4 has-text-centered fo-w-l pa-one pa-no-b">¿Estas seguro de querer borrar el filtro?</h2>';
          content += '<h3 class="is-size-6 has-text-centered fo-w-l pa-b-one">Esta accion es irreversible</h3>';
          content += '<a class="button is-danger confirm-borrar">Borrar</a>';
        content += '</div>';
        $('.modal-content').html(content);
        //FILTRO BORRADO
        $('.confirm-borrar').on('click', function(e){
          $.ajax({
            type: "POST",
            url: "php/main/main-filter-delete.php",
            data: {
              auth : stringaUth_key,
              user : stringaUth_user,
              num : num
            },
            async:true,
            dataType : 'json',
            crossDomain: true,
            context: document.body,
            cache: false,
            success: function(data) {
              $('.modal').remove();
              carga_info_filtros();
            },
            error: function(xhr, tst, err) {
              alert('El tiempo de espera fue superado, por favor intentalo en un momento mas');
              closeall();
            },
            timeout: 10000
          })
        })
      })
      cierra_loader();
    },
    error: function(xhr, tst, err) {
      alert('El tiempo de espera fue superado, por favor intentalo en un momento mas');
      closeall();
    },
    timeout: 10000
  })
}

//EDITA INFORMACIÓN DE USUARIO
$('.editUserPerfil').on('click', function() {
  carga_modal();
  $('.modal-content').load('templates/user/user-edit.html');
})
