$(document).ready(function() {

  //ACTIONS TABS
  $('.actionMainCompass').on('click', function(e){
    $('.overlay-principal').html('');
    $(".navbar-menu").removeClass("is-active");
    $('.overlay-principal').load('templates/main/main-scroll.html');
  })

  $('.actionMainWorld').on('click', function(e){
    carga_main_body();
    $(".navbar-menu").removeClass("is-active");
    $('.overlay-principal').html('')
  })

  $('.actionMainUser').on('click', function(e){
    $('.overlay-principal').html('');
    $(".navbar-menu").removeClass("is-active");
    localStorage.removeItem("edit_id");
    localStorage.removeItem("proshow");
    $('.overlay-principal').load('templates/main/main-user.html');
  })

  translate();

});
