<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

  //VIENE DE CATEGORIAS
  $consulta_final = "SELECT pro_index, cate_index, nom, pre, cur, ren FROM pro_br ORDER BY RAND()";
  if ($_POST['renta'] == 3) {
    if (!empty($_POST['cate_index'])) {
      $consulta_final = "SELECT pro_index, cate_index, nom, pre, cur, ren FROM pro_br WHERE cate_index = '".$_POST['cate_index']."' ORDER BY RAND()";
    } elseif (!empty($_POST['perf_index'])) {
      $consulta_final = "SELECT pro_index, cate_index, nom, pre, cur, ren FROM pro_br WHERE perf_index = '".$_POST['perf_index']."' ORDER BY RAND()";
    }
  }
  if ($_POST['renta'] == 0 || $_POST['renta'] == 1) {
    if (!empty($_POST['cate_index'])) {
      $consulta_final = "SELECT pro_index, cate_index, nom, pre, cur, ren FROM pro_br WHERE ren = ".$_POST['renta']." AND cate_index = '".$_POST['cate_index']."' ORDER BY RAND()";
    } elseif (!empty($_POST['perf_index'])) {
      $consulta_final = "SELECT pro_index, cate_index, nom, pre, cur, ren FROM pro_br WHERE ren = ".$_POST['renta']." AND perf_index = '".$_POST['perf_index']."' ORDER BY RAND()";
    }
  }

  $sqlPro =  $mysqli->query($consulta_final);
  if ($sqlPro->num_rows > 0) {
    while ($row = $sqlPro->fetch_assoc()) {
      //LEEMOS LA CATEGORIA
      $sqlcategoria = $mysqli->query("SELECT nom FROM cate_br WHERE cate_index = '".$row['cate_index']."'");
      if ($sqlcategoria->num_rows > 0) {
        $rowcategoria = $sqlcategoria->fetch_assoc();
        $categoria = $rowcategoria['nom'];
      }
      //LEEMOS DIRECCION
      $sql_dir =  $mysqli->query("SELECT col, ciu, est, pai FROM dire_br WHERE pro_index = '".$row['pro_index']."'");
      if ($sql_dir->num_rows > 0) {
        $row_dir = $sql_dir->fetch_assoc();
        //LEE JSON CONFIG
        $filename = file_get_contents('../../assets/opc_br/'.$row['pro_index'].'_opc.json');
        $data = json_decode($filename, true);
        //RESULTADOS
        $resultados[] = array("success"=>true, 'pro_index'=>$row['pro_index'], 'nom'=>$row['nom'], "ren"=>$row['ren'], "pre"=>$row['pre'], "cur"=>$row['cur'], "cate_index"=>$row['cate_index'], "cat"=>$categoria, "col"=>$row_dir['col'], "ciu"=>$row_dir['ciu'], "est"=>$row_dir['est'], "pai"=>$row_dir['pai'], 'cuartos'=>$data[0]['cuartos'], 'jacuzzi'=>$data[0]['jacuzzi'], 'air'=>$data[0]['air'], "jardin"=>$data[0]['jardin'], "trasero"=>$data[0]['trasero'], "chimenea"=>$data[0]['chimenea'], "banos"=>$data[0]['banos'], "cochera"=>$data[0]['cochera'], "piscina"=>$data[0]['piscina'], "terraza"=>$data[0]['terraza'], "balcon"=>$data[0]['balcon'], "seguridad"=>$data[0]['seguridad']);
      } else{
        $resultados[] = array("success"=>false, "error"=>'Error, en direccion');
      }
    }
  } else {
    $resultados[] = array("success"=>false, "error"=>'Error, no resultados');
  }

  print json_encode($resultados);

  include('../../functions/cierra_conexion.php');
?>
