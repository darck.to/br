<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

  $sqlcate =  $mysqli->query("SELECT cate_index, nom FROM cate_br");
  if ($sqlcate->num_rows > 0) {
    while ($rowcate = $sqlcate->fetch_assoc() ) {
      $sqlcount =  $mysqli->query("SELECT COUNT(id) as number FROM pro_br WHERE cate_index = '".$rowcate['cate_index']."'");
      if ($sqlcount->num_rows > 0) {
        $rowcount = $sqlcount->fetch_assoc();
        //LEE IMG
        $img = "assets/cat_img/" . $rowcate['cate_index'] . ".png";
        $resultados[] = array("success"=>true, "cate_index"=>$rowcate['cate_index'], "nom"=>$rowcate['nom'], "img"=>$img, "num"=>$rowcount['number']);
      }
    }

  } else {
    $resultados[] = array("success"=>false, "error"=>"There was an error, please contact support");
  }

  print json_encode($resultados);

  include('../../functions/cierra_conexion.php');
?>
