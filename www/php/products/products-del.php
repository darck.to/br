<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');

  // "limpiamos" los campos del formulario de posibles códigos maliciosos
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $pro_index = $_POST['pro_index'];

  $sql =  $mysqli->query("SELECT init_index, nom FROM init_auth WHERE auth_number = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    //ingresamos los datos a la BDD PRO_BR
    $sqlPro = $mysqli->query("DELETE FROM pro_br WHERE pro_index = '".$pro_index."'");
    if($sqlPro) {
      //ingresamos los datos a la BDD DIRE_BR
      $sqlDire = $mysqli->query("DELETE FROM dire_br WHERE pro_index = '".$pro_index."'");
      if($sqlDire) {
        //ingresamos los datos a la BDD DESC_BR
        $sqlDesc = $mysqli->query("DELETE FROM desc_br WHERE pro_index = '".$pro_index."'");
        if($sqlDesc) {
          //ELIMINAMOS EL ARCHIVO DE CONFIGURACION
        	$fileName = '../../assets/opc_br/'.$pro_index.'_opc.json';
        	if (file_exists($fileName)) {
            unlink($fileName);
        	}
          //GUARDAMOS LAS IMAGENES DEL PRODUCTO EN SU DESTINO FINAL, DE EXISTIR
          $carpeta = "../../assets/pro_img/".$pro_index;
          $fileList = glob($carpeta. "/" . $pro_index . "_*.png");
        	//RECORREMOS LOS ARCHIVOS SI TOMO FOTOS NUEVAS
          $n = 0;
        	foreach($fileList as $filename){
            if ( file_exists($carpeta."/".$pro_index."_" . $n . ".png") ) {
              unlink($carpeta."/".$pro_index."_" . $n . ".png");
              $n++;
            }
        	}
          $resultados[] = array("success"=> true, "info"=>"Producto elimanado");
        } else {
          $resultados[] = array("success"=> false, "error"=> "Error en eliminacion de descripcion, contact support");
        }
      } else {
        $resultados[] = array("success"=> false, "error"=> "Error en eliminacion de direccion, contact support");
      }
    } else {
      $resultados[] = array("success"=> false, "error"=> "Error general, contact support");
      //$resultados[] = array("success"=> false, "error"=> mysqli_error($mysqli));
    }
  } else {
    $resultados[] = array("success"=> false);
  }

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../functions/cierra_conexion.php');
?>
