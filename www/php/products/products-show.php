<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

  $pro_index = mysqli_real_escape_string($mysqli,$_POST['pro_index']);

  $sql_pro =  $mysqli->query("SELECT nom, lat, lng, ren, pre, cur, cate_index, perf_index FROM pro_br WHERE pro_index = '".$pro_index."'");
  if ($sql_pro->num_rows > 0) {
    $row_pro = $sql_pro->fetch_assoc();

    $sql_per =  $mysqli->query("SELECT nom, ape, mat, tel, cel FROM perf_br WHERE perf_index = '".$row_pro['perf_index']."' ");
    if ($sql_per->num_rows > 0) {
      $row_per = $sql_per->fetch_assoc();

      //CORREO DEL USUARIO DUENO
      $sql_due =  $mysqli->query("SELECT mai FROM init_auth WHERE init_index = '".$row_pro['perf_index']."' ");
      if ($sql_due->num_rows > 0) {
        $row_due = $sql_due->fetch_assoc();
      }

      //LEEMOS LA CATEGORIA
      $sqlcategoria = $mysqli->query("SELECT nom FROM cate_br WHERE cate_index = '".$row_pro['cate_index']."'");
      if ($sqlcategoria->num_rows > 0) {
        $rowcategoria = $sqlcategoria->fetch_assoc();
        $categoria = $rowcategoria['nom'];
      }

      $sql_dir =  $mysqli->query("SELECT cal, num, col, ciu, cp, est, pai FROM dire_br WHERE pro_index = '".$pro_index."'");
      if ($sql_dir->num_rows > 0) {
        $row_dir = $sql_dir->fetch_assoc();

        $sql_des =  $mysqli->query("SELECT des FROM desc_br WHERE pro_index = '".$pro_index."'");
        if ($sql_des->num_rows > 0) {
          $row_des = $sql_des->fetch_assoc();

          //LEE JSON CONFIG
          $filename = file_get_contents('../../assets/opc_br/'.$pro_index.'_opc.json');
          $data = json_decode($filename, true);

          //NOMBRE DE ARCHIVO
          $fileList = glob('../../assets/pro_img/'.$pro_index.'/*.png');
          $n = 0;
          //RECORREMOS LOS ARCHIVOS
          foreach($fileList as $filename){
            //SI SON ARCHIVOS PNG LOS CONTAMOS
            if (file_exists($filename)) {
              $n++;
            }
          }

          $resultados[] = array("success"=>true, "usu"=>$row_per['nom']." ".$row_per['ape']." ".$row_per['mat'], "mai"=>$row_due['mai'], "tel"=>$row_per['tel'], "cel"=>$row_per['cel'], "nom"=>$row_pro['nom'], "lat"=>$row_pro['lat'], "lng"=>$row_pro['lng'], "ren"=>$row_pro['ren'], "pre"=>$row_pro['pre'], "cur"=>$row_pro['cur'], "cat"=>$categoria, "cate_index"=>$row_pro['cate_index'], "perf_index"=>$row_pro['perf_index'], "cal"=>$row_dir['cal'], "num"=>$row_dir['num'], "col"=>$row_dir['col'], "ciu"=>$row_dir['ciu'], "cp"=>$row_dir['cp'], "est"=>$row_dir['est'], "pai"=>$row_dir['pai'], "des"=>$row_des['des'], 'cuartos'=>$data[0]['cuartos'], 'jacuzzi'=>$data[0]['jacuzzi'], 'air'=>$data[0]['air'], "jardin"=>$data[0]['jardin'], "trasero"=>$data[0]['trasero'], "chimenea"=>$data[0]['chimenea'], "banos"=>$data[0]['banos'], "cochera"=>$data[0]['cochera'], "piscina"=>$data[0]['piscina'], "terraza"=>$data[0]['terraza'], "balcon"=>$data[0]['balcon'], "seguridad"=>$data[0]['seguridad'], "multimedia"=>$n);

        }
      }
    }
  } else {
    $resultados[] = array("success"=>false);
  }

  print json_encode($resultados);

  include('../../functions/cierra_conexion.php');
?>
