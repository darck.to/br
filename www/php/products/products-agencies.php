<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

  $sql_pro =  $mysqli->query("SELECT nom, perf_index FROM perf_br WHERE plus_index = 2 LIMIT 8");
  if ($sql_pro->num_rows > 0) {
    while ($row_pro = $sql_pro->fetch_assoc()) {
      $perf_index =$row_pro['perf_index'];
      $nom =$row_pro['nom'];
      $avatar = 'assets/perf_img/' . $perf_index . '.png';
      //if (file_exists($avatar)) {$avatar = 'assets/perf_img/' . $perf_index . '.png';} else {$avatar = 'assets/perf_img/avatar.png';}
      $resultados[] = array("success"=>true, "perf_index"=>$perf_index, "img"=>$avatar, "nom"=>$nom);
    }
  } else {
    $resultados[] = array("success"=>false);
  }

  print json_encode($resultados);

  include('../../functions/cierra_conexion.php');
?>
