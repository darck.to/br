<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');

  if ( empty($_POST['mai']) ) {
    echo "No has ingresado el usuario!";
  } else {
    $mai = mysqli_real_escape_string($mysqli,$_POST['mai']);
    $sql = $mysqli->query("SELECT nom, mai FROM init_auth WHERE mai = '$mai'");
    if ($sql->num_rows > 0) {
      $row = $sql->fetch_assoc();
      $num_caracteres = 12; // asignamos el número de caracteres que va a tener la nueva contraseña
      $nueva_clave = substr(md5(rand()),0,$num_caracteres); // generamos una nueva contraseña de forma aleatoria
      $usuario_nombre = $row['nom'];
      $usuario_clave = $nueva_clave; // la nueva contraseña que se enviará por correo al usuario
      $usuario_clave2 = md5($usuario_clave); // encriptamos la nueva contraseña para guardarla en la BD
      $usuario_email = $row['mai'];
      // actualizamos los datos (contraseña) del usuario que solicitó su contraseña
      if ($mysqli->query("UPDATE init_auth SET pas = '$usuario_clave2' WHERE mai = '$mai'")) {
        // Enviamos por email la nueva contraseña
        $remite_nombre = "search2me"; // Tu nombre o el de tu página
        $remite_email = "no-reply@search2me.com"; // tu correo
        $asunto = "Password recovery"; // Asunto (se puede cambiar)
        $mensaje = "A new password for username: <strong>".$usuario_nombre."</strong>. New password is: <strong>" . $usuario_clave . "</strong>.";
        $cabeceras = "From: ".$remite_nombre." <".$remite_email.">\r\n";
        $cabeceras = $cabeceras."Mime-Version: 1.0\n";
        $cabeceras = $cabeceras."Content-Type: text/html";
        $enviar_email = mail($usuario_email,$asunto,$mensaje,$cabeceras);
        if ($enviar_email) {
          $resultados[] = array("success"=>true, "message"=>"Sent to: " . $usuario_email);
        } else {
          $resultados[] = array("success"=>false, "message"=>"Error, contact support");
        }
      } else {
        $resultados[] = array("success"=>false, "message"=>"Error, bdd error contact support");
        //printf("<br>Errormessage: %s\n", $mysqli->error);
      }
    } else {
      $resultados[] = array("success"=>true, "message"=>"User not found");
    }
  }

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../functions/cierra_conexion.php');

?>
