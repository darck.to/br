<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $sql_auth =  $mysqli->query("SELECT init_index, nom FROM init_auth WHERE auth_number = '".$auth."' AND nom = '".$user."' ");
  if ($sql_auth->num_rows > 0) {
    $row = $sql_auth->fetch_assoc();
    $init_index = $row['init_index'];

    // Set new file name
    $new_image_name = "../../assets/perf_img/".$init_index.".png";
    if (file_exists($new_image_name)) {
      unlink($new_image_name);
    }
    // upload file
    if (move_uploaded_file($_FILES["file"]["tmp_name"], $new_image_name)) {
      $resultados[] = array("success"=>true, "img"=>$new_image_name);
    } else {
        $resultados[] = array("error"=>'El archivo no pudo subirse, por favor contacta soporte');
    }

  } else {
    $resultados[] = array("error"=>'Error, por favor contacta soporte');
  }

  print json_encode($resultados);

  include('../../functions/cierra_conexion.php');
?>
