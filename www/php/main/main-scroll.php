<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);
  $lat = mysqli_real_escape_string($mysqli,$_POST['lat']);
  $lng = mysqli_real_escape_string($mysqli,$_POST['lng']);
  $dis = mysqli_real_escape_string($mysqli,$_POST['dis']);

  $sql_auth =  $mysqli->query("SELECT init_index FROM init_auth WHERE auth_number = '".$auth."' AND nom = '".$user."' ");
  if ($sql_auth->num_rows > 0) {
    $row = $sql_auth->fetch_assoc();

    $sqlCo =  $mysqli->query("SELECT pro_index, nom FROM (SELECT *, (((acos(sin((".$lat."*pi()/180)) * sin((lat*pi()/180))+cos((".$lat."*pi()/180)) * cos((lat*pi()/180)) * cos(((".$lng."- lng)*pi()/180))))*180/pi())*60*1.1515*1.609344) as distance FROM pro_br)pro_br WHERE distance <= $dis LIMIT 50 ");
    if ($sqlCo->num_rows > 0) {
      while ($rowCo = $sqlCo->fetch_assoc()) {

        //LEE JSON CONFIG
        $filename = file_get_contents('../../assets/opc_br/'.$rowCo['pro_index'].'_opc.json');
        $data = json_decode($filename, true);

        $resultados[] = array("success"=>true,'pro_index'=>$rowCo['pro_index'], 'nom'=>$rowCo['nom'], "cuartos"=>$data[0]['cuartos'], "jacuzzi"=>$data[0]['jacuzzi'], "air"=>$data[0]['air'], "jardin"=>$data[0]['jardin'], "trasero"=>$data[0]['trasero'], "chimenea"=>$data[0]['chimenea'], "banos"=>$data[0]['banos'], "cochera"=>$data[0]['cochera'], "piscina"=>$data[0]['piscina'], "terraza"=>$data[0]['terraza'], "balcon"=>$data[0]['balcon'], "seguridad"=>$data[0]['seguridad'], "recepcion"=>$data[0]['recepcion'], "gimnasio"=>$data[0]['gimnasio']);
      }
    } else {
      $resultados[] = array("success"=>false, "error"=>'Error, por favor contacta soporte');
    }

    print json_encode($resultados);

  } else {
    print json_encode('Error');
  }

  include('../../functions/cierra_conexion.php');
?>
