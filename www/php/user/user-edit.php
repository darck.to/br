<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');

  // "limpiamos" los campos del formulario de posibles códigos maliciosos
  $auth = mysqli_real_escape_string($mysqli, $_POST['key']);
  $user = mysqli_real_escape_string($mysqli, $_POST['user']);
  $usu = mysqli_real_escape_string($mysqli, $_POST['usu']);
  $mai = mysqli_real_escape_string($mysqli, $_POST['mai']);
  $pas = mysqli_real_escape_string($mysqli, $_POST['pas']);
  $nom = mysqli_real_escape_string($mysqli, $_POST['nom']);
  $ape = mysqli_real_escape_string($mysqli, $_POST['ape']);
  $mat = mysqli_real_escape_string($mysqli, $_POST['mat']);
  $tel = mysqli_real_escape_string($mysqli, $_POST['tel']);
  $cel = mysqli_real_escape_string($mysqli, $_POST['cel']);

  $password = "";

  // comprobamos que se vaya a modificar la contraseña
  if (empty($pas)) {} else {
    $pas = md5($pas);
    $sqlPass = $mysqli->query("UPDATE init_auth SET pas = '".$pas."' WHERE auth_number = '".$auth."' AND nom = '".$user."' ");
    if($sqlPass) {
      $password = ", password cambiado";
    } else {
      $password = ", hubo un error al cambiar el password, contacta a soporte";
    }
  }

  //OBTENEMOS INIT_INDEX
  $sqLogin =  $mysqli->query("SELECT init_index FROM init_auth WHERE auth_number = '".$auth."' AND nom = '".$user."' ");
  if ($sqLogin->num_rows > 0) {
    $rowLogin = $sqLogin->fetch_assoc();
    $init_index = $rowLogin["init_index"];
  } else {
  }

  // ingresamos los datos a la BD TLABLA INDEX
  $sqlPerf = $mysqli->query("UPDATE perf_br SET nom = '".$nom."', ape = '".$ape."', mat = '".$mat."', tel = '".$tel."', cel = '".$cel."' WHERE perf_index = '".$init_index."' ");
  if($sqlPerf) {
    $sqlInit = $mysqli->query("UPDATE init_auth SET nom = '".$usu."', mai = '".$mai."' WHERE auth_number = '".$auth."' AND nom = '".$user."' ");
    if($sqlInit) {
      $resultados[] = array("success"=> true, "error"=>"Datos Modificados".$password);
    } else {
      $resultados[] = array("success"=> false, "error"=> "Error, contact support".$password);
      //$resultados[] = array("success"=> false, "error"=> mysqli_error($mysqli));
    }
  }

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../functions/cierra_conexion.php');
?>
